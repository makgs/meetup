<?php

use Phinx\Migration\AbstractMigration;

class CreateEventsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     
    public function change()
    {

    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $rows = $this->query( "CREATE TABLE `events` (
            `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(255) NOT NULL,
            `description` VARCHAR(1000) NOT NULL,
            `user_id` INT(11) UNSIGNED NOT NULL,
            `date_start` DATE NOT NULL,
            `date_end` DATE NOT NULL,
            `is_finished` TINYINT(1) NOT NULL DEFAULT '0',
            `sity` VARCHAR(255) NOT NULL,
            `address` VARCHAR(255) NOT NULL,
            PRIMARY KEY (`id`)
            )"
        );
    }
    
    /**
     * Migrate Down.
     */
    public function down() {
        $this->dropTable('events');
    }
}
