<?php

use Phinx\Migration\AbstractMigration;

class CreateUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     
    public function change() {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $rows = $this->query( 'CREATE TABLE `users` (
            `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
            `first_name` VARCHAR(255) NOT NULL,
            `last_name` VARCHAR(255) NOT NULL,
            `email` VARCHAR(255) NOT NULL,
            `vk_key` INT(11) UNSIGNED NOT NULL,
            `gender` VARCHAR(1) NOT NULL,
            `birthday` DATE NOT NULL,
            `role` VARCHAR(255) NOT NULL,
            PRIMARY KEY (`id`)
            )'
        );
    }
    
    /**
     * Migrate Down.
     */
    public function down() {
        $this->dropTable('users');
    }
}
