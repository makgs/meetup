<?php

use Phinx\Migration\AbstractMigration;

class CreateSubscriptionsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
     
    /**
     * Migrate Up.
     */
    public function up()
    {
        $rows = $this->query( 'CREATE TABLE `subscriptions` (
            `user_id` INT(11) UNSIGNED NOT NULL,
            `event_id` INT(11) UNSIGNED NOT NULL,
            `id` INT(15) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT
            )'
        );
        $this->query( 'ALTER TABLE `events`
             ADD `subscription_count` INT(7) UNSIGNED NOT NULL'
        );
    }
    
    /**
     * Migrate Down.
     */
    public function down() 
    {
        $this->dropTable('subscriptions');
        $this->query( 'ALTER TABLE `events`
             DROP `subscription_count`'
        );
    }
}
