<?php

use Phinx\Migration\AbstractMigration;

class DeleteA2ImagePluginAndAddUploadPluginFields extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->dropTable('images');
        $this->dropTable('images_relations');
        
        $this->query( 'ALTER TABLE `events` DROP `image_id`');
        
        $this->query( 'ALTER TABLE `events`
             ADD `photo` varchar(255)  DEFAULT NULL'
        );
        $this->query( 'ALTER TABLE `events`
             ADD `photo_dir` varchar(255)  DEFAULT NULL'
        );
        
    }
        public function down()
    {
        $this->query("CREATE TABLE `images` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `foreign_key` int(10) unsigned DEFAULT NULL,
              `model` varchar(255) NOT NULL DEFAULT '',
              `filename` varchar(255) NOT NULL DEFAULT '',
              `dir` int(11) unsigned DEFAULT NULL,
              `order` int(11) unsigned DEFAULT NULL,
              `is_slider` int(2) unsigned DEFAULT NULL,
              `site_id` int(11) unsigned DEFAULT NULL,
              PRIMARY KEY (`id`)
            );
            CREATE TABLE `images_relations` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `foreign_id` int(11) unsigned NOT NULL,
              `foreign_name` varchar(255) NOT NULL,
              `image_id` int(11) unsigned NOT NULL,
              PRIMARY KEY (`id`)
            );"
        );
        
        $this->query( 'ALTER TABLE `events`
             ADD `image_id` INT(10) UNSIGNED DEFAULT NULL'
        );
        
        $this->query( 'ALTER TABLE `events` DROP `photo`');
        
        $this->query( 'ALTER TABLE `events` DROP `photo_dir`');
    }
}
