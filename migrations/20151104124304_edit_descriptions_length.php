<?php

use Phinx\Migration\AbstractMigration;

class EditDescriptionsLength extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->query( 'ALTER TABLE `events`
            CHANGE `description` `description` VARCHAR(20000);'
        );
        $this->query( 'ALTER TABLE `actions`
            CHANGE `description` `description` VARCHAR(5000);'
        ); 
    }
    public function down()
    {
        $this->query( 'ALTER TABLE `events`
            CHANGE `description` `description` VARCHAR(1000);'
        );
        $this->query( 'ALTER TABLE `actions`
            CHANGE `description` `description` VARCHAR(1000);'
        ); 
    }
}
