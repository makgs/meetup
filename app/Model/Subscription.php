<?php
App::uses('AppModel', 'Model');
/**
 * Subscription Model
 *
 * @property User $User
 * @property Event $Event
 */
class Subscription extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'event_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);


/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Event' => array(
			'counterCache' => true,
			'className' => 'Event',
			'foreignKey' => 'event_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
/**
 * returned true, if user is signed
 *
 * @param int $eventId Id of event
 * @param int $userId Id of user
 *
 * @return true|false
 */    
	public function isSubscription($eventId, $userId) {
                
        $subscriptionOptions = array(
            'recursive' => -1,
            'conditions' => array(
                'Subscription.event_id' => $eventId,
                'Subscription.user_id' => $userId,
            )
        );
        
        return (bool)$this->find('first', $subscriptionOptions);
	}
/**
 * returned array of events id
 *
 * @param int $eventId Id of event
 * @param int $userId Id of user
 *
 * @return true|false
 */    
	public function getUserSubscription($userId) {
                
        $subscriptionOptions = array(
            'recursive' => -1,
            'conditions' => array(
                'Subscription.user_id' => $userId,
            ),
            'fields' => array('Subscription.event_id')
        );
        $result = array();
        
        return $this->find('list', $subscriptionOptions);
	}
}
