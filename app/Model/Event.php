<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 * @property User $User
 * @property Action $Action
 */
class Event extends AppModel {
    
    public $actsAs = array(
        'Upload.Upload' => array(
            'photo' => array(
                'fields' => array(
                    'dir' => 'photo_dir'
                ),
                'deleteFolderOnDelete'  => true,
                'deleteOnUpdate'        => true,
                'thumbnailSizes' => array(
                    'small' => '1150x1150',
                    'big' => '1920x1280',
                )
            )
        )
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
        'date_end' => array(
            'rule' => array('dateEndCheck'),
            'message' => 'Дата окончания более ранняя, чем дата начала'
        ),
	);
    /**
     * compares date of end and date of start
     */
    public function dateEndCheck($check) {

        $dateStart = strtotime($this->data[$this->alias]['date_start']);
        $dateEnd = strtotime($check['date_end']);
        if (empty($check['date_end'])) {
            return true;
        } else {
            return ($dateEnd > $dateStart);
        }
    }
/**
 *  Convert special characters to HTML entities
 */
    public function beforeSave($options = Array()) {
        
		if (isset($this->data[$this->alias]['name'])) {
            
			$this->data[$this->alias]['name'] = htmlspecialchars($this->data[$this->alias]['name']);
            
		}        
		if (isset($this->data[$this->alias]['description'])) {
            
			$this->data[$this->alias]['description'] = htmlspecialchars($this->data[$this->alias]['description']);
            
		}
		if (isset($this->data[$this->alias]['city'])) {
            
			$this->data[$this->alias]['city'] = htmlspecialchars($this->data[$this->alias]['city']);
            
		}
		if (isset($this->data[$this->alias]['address'])) {
            
			$this->data[$this->alias]['address'] = htmlspecialchars($this->data[$this->alias]['address']);
            
		}        
		return true;        
	}
    
	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Action' => array(
			'className' => 'Action',
			'foreignKey' => 'event_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
        'Subscription' => array(
			'className' => 'Subscription',
			'foreignKey' => 'event_id',
			'dependent' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

/**
 * returned true if event is owned by user
 *
 * @param int $eventId Id of event
 * @param int $userId Id of user
 *
 * @return true|false Evten is owned by user or not
 */    
	public function isOwnedBy($eventId, $userId) {
        
        if ($this->field('id', array('id' => $eventId, 'user_id' => $userId)) !== false) {
            return true;
        } else {
            return false;
        }
	}	 

/**
 * returned side bar's events
 *
 * @param int $eventId Id of event
 *
 * @return array Evtens array of events
 */    
	public function getSideBarEvents($eventId = null) {
        
        $today = date("Y-m-d H:i:s");
        
        $sideBarOptions = array(
            'recursive' => -1,
            'conditions' => array(
                'Event.is_public' => 1,
                'Event.date_start >' => $today,
                'Event.' . $this->primaryKey . ' !=' => $eventId,
            ),
            'order' => array('Event.date_start' => 'asc'),
            'limit' => 3
        );
        
        return $this->find('all', $sideBarOptions);
	}
/**
 * returned number of subscriptions
 *
 * @param int $eventId Id of event
 *
 * @return int Numberof subscriptions
 */    
	public function getNumSubscription($eventId) {
        
        $sideBarOptions = array(
            'recursive' => -1,
            'conditions' => array(
                'Event.date_start >' => $today,
                'Event.' . $this->primaryKey . ' !=' => $eventId,
            ),
            'order' => array('Event.date_start' => 'asc'),
            'limit' => 3
        );
        
        return $this->find('all', $sideBarOptions);
	} 
}
