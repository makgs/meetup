$(document).ready(function() {        
	$('.datetimepicker').datetimepicker({
		weekStart: '1',
        language: 'ru',
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true
    });
});