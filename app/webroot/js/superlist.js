$(document).ready(function() {
    'use strict';

    /**
     * Background image
     */
    $('*[data-background-image]').each(function() {
        $(this).css({
            'background-image': 'url(' + $(this).data('background-image') + ')'
        });
    });

    /**
     * Bootstrap Tooltip
     */
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    /**
     * Listing Detail Bookmark & Like
     */
    $(".detail-banner-btn").click(function(){
        $(this).toggleClass("marked");

        var span = $(this).children("span");
        var toggleText = span.data("toggle");
        span.data("toggle", span.text());
        span.text(toggleText);

    });

    /**
     * Textarea resizer
     */
    $("textarea").after('<div class="textarea-resize"></div>');

    /**
     * Add, Delete, Reset input in form "Create event"
    */

    var i = $('.ft_inputs').size();
    var j = $('.dt_inputs').size();

    $('#add_fixed_theme').click(function() {
        i = $('.ft_inputs:last').attr('id');
        ++i;
        $('<div id="'+ i +'" class="ft_inputs">\n' +
            '<input type="text" class="ft_input form-control" required="required" name="ft[name]['+ i +']" placeholder="Название темы"/>\n' +
            '<textarea class="ft_input form-control" rows="5" name="ft[text]['+ i +']" placeholder="Описание темы"></textarea>\n' +
            '<span class="remove_fixed_theme btn btn-danger">Удалить тему</span>\n' +
            '</div>') .
        fadeIn('slow').appendTo('.fixed_theme');
    });

    $('.fixed_theme').on('click', '.remove_fixed_theme', function(){
        if (confirm("Вы хотите удалить тему?") == true) {
            $(this).closest('.ft_inputs').remove();
            i--;
        }
    });


    $('#reset_fixed_theme').click(function() {
    while( $('.ft_inputs').size() > 1) {
        $('.ft_inputs:last').remove();
        i--;

        $('html, body').stop().animate({
            scrollLeft: 0, 
            scrollTop:$('h2.add_fix').offset().top
        }, 100); 
    }
    });


    $('#add_theme').click(function() {
        j = $('.dt_inputs:last').attr('id');
        j++;
        $('<div id="'+ j +'" class="dt_inputs">\n' +
            '<input type="text" class="dt_input form-control" required="required" name="dt[name]['+ j +']" placeholder="Название темы"/>\n' +
            '<textarea class="dt_input form-control" rows="5" name="dt[text]['+ j +']" placeholder="Описание темы"></textarea>\n' +
            '<span class="remove_theme btn btn-danger">Удалить тему</span>\n' +
            '</div>').fadeIn('slow').appendTo('.discussed_theme');
    });

    $('.discussed_theme').on('click', '.remove_theme', function(){
        if (confirm("Вы хотите удалить тему?") == true) {
            $(this).closest('.dt_inputs').remove();
            j--;
        }    
    });

    $('#reset_theme').click(function() {
    while( $('.dt_inputs').size() > 1) {
        $('.dt_inputs:last').remove();
        j--;

        $('html, body').stop().animate({
            scrollLeft: 0, 
            scrollTop:$('h2.add_disc').offset().top
            }, 100); 
        }
    });
    
    if ($(window).width() < 769) {
        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 700) {
                    $('#back_top_panel').show();
                    $('#back_top_panel').fadeIn();
                } else {
                    $('#back_top_panel').fadeOut();
                }
            });

            $('#back_top_panel').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                    }, 800);
                return false;

            });
        });
    } else {
        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 700) {
                    $('#back-top').show();
                    $('#back-top').fadeIn();
                } else {
                    $('#back-top').fadeOut();
                }
            });

            $('#back-top i').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                    }, 800);
                return false;
            });
        });
    }

    $('.like_ft_thumbs').click(function() {
        var this_parent = $(this).parent();
        var like_class = this_parent.find('.like_heart').attr('class');
        var str_reg = like_class.indexOf('dont_reg', 0);
        var voting_count;
        var action_id_fix = this_parent.find('span.all_likes').data('fixlike');

        function ajax_call(this_parent) {
            $.ajax({
                url: "/Votings/index",
                type: "POST",
                data: {param: action_id_fix},
                success: function(data) {
                    data = jQuery.parseJSON(data);
                    voting_count = data.voting_count;
                    this_parent.find('.all_likes').text(voting_count);
                }, 
                error: function(req, text, error) {
                    alert('Ошибка сервера!' + ' ' + text + ' ' + error);
                },
            });
        }

        if (str_reg < 0) {

            var str_active = like_class.indexOf('active', 0);

            if (str_active < 0 ) {
                this_parent.find('.like_heart').addClass('active');
                ajax_call(this_parent);

            } else {
                this_parent.find('.like_heart').removeClass('active');
                ajax_call(this_parent);

            }
        } else {
            this_parent.find('.like_ft_thumbs').attr('data-target', '#modal-reg');
        }   
    });

    $('.like_dt_thumbs').click(function() {
        var this_parent = $(this).parent();
        var like_class = this_parent.find('.like_heart').attr('class');
        var voting_count;
        var str_reg = like_class.indexOf('dont_reg', 0);
        var action_id = this_parent.find('span.all_likes').data('like');

        function ajax_call_dt(this_parent) {
            $.ajax({
                url: "/Votings/index",
                type: "POST",
                data: {param: action_id},
                success: function(data) {
                    data = jQuery.parseJSON(data);
                    voting_count = data.voting_count;
                    this_parent.find('.all_likes').text(voting_count);
                }, 
                error: function(req, text, error) {
                    alert('Ошибка сервера!' + ' ' + text + ' ' + error);
                },
            });
        }

        if (str_reg < 0) {

            var str_active = like_class.indexOf('active', 0);
            
            if (str_active < 0) {
                this_parent.find('.like_heart').addClass('active');
                ajax_call_dt(this_parent);  

            } else {
                this_parent.find('.like_heart').removeClass('active');
                ajax_call_dt(this_parent);

            }

        } else {  
            this_parent.find('.like_dt_thumbs').attr('data-target', '#modal-reg');
        }   
    });
    
    /**
     * Subscription - start
     *
     * uses in /admin/events/subscriptions and /events/view/(id)
     */ 
     
    $('#subscribe').click(function() {
        
        var eventId = $(this).attr('data-event-id');
        
        $.ajax({
            url: "/subscriptions/subscribe",
            type: "POST",
            data: {'event': eventId},
            success: function(data) {
                data = jQuery.parseJSON(data);
                var followers_count = data.supscription_count;
                $('#followers_count').text(followers_count);
            }, 
            error: function(req, text, error) {
                alert('Ошибка сервера!' + ' ' + text + ' ' + error);
            },
        });
    });
     
    /**
     * Subscription - end
     */

    $(".tab").click(function() {
        $(".tab").removeClass("active").eq($(this).parent().index()).addClass("active");
        $(".tab_item").hide().eq($(this).parent().index()).fadeIn()
    }).eq(0).addClass("active"); 

    $('#accordion .panel-collapse.collapse:first').addClass('in');
    $('#accordion-1 .panel-collapse.collapse:first').addClass('in');

});