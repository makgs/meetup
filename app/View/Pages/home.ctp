<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */

if (!Configure::read('debug')):
	throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>

<div class="content">
    <div class="mt-80">
        <div class="hero-image">
            <div class="hero-image-inner">
                <div class="hero-image-content">
                    <div class="container">
                        <h1>Yourmeetup</h1>
                        <p>Создавайте свои события, предлагайте темы, <br>утверждайте понравившиеся, следите за реакцией аудитории.</p>
                    </div><!-- /.container -->
                </div><!-- /.hero-image-content -->

            </div><!-- /.hero-image-inner -->
        </div><!-- /.hero-image -->
    </div>

    <div class="container">


    </div><!-- /.container -->
</div>