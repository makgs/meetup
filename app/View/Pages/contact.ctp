<div class="container">
    <div class="content">
        <p>
            Donec congue egestas nisi id varius. Curabitur ullamcorper consectetur risus, eget dapibus lacus. Maecenas sit amet dui dictum, tincidunt ante vel, semper purus. In hac habitasse platea dictumst. Vivamus hendrerit sem a rutrum ornare. Donec vehicula auctor eros. Etiam et enim tellus.
        </p>

        <div class="row">
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-4">
                        <h3>Address</h3>
                        <p>
                            13/2 Elizabeth Street,<br>
                            Melbourne, Australia
                        </p>
                    </div><!-- /.col-sm-4 -->

                    <div class="col-sm-4">
                        <h3>Contacts</h3>
                        <p>
                            1900-CO-WORKER<br>
                            1800-3322-4453
                        </p>
                    </div><!-- /.col-sm-4 -->

                    <div class="col-sm-4">
                        <h3>Social Profiles</h3>

                        <ul class="social-links nav nav-pills">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div><!-- /.col-sm-4 -->
                </div><!-- /.row -->

                <div class="background-white p30 mt30">
                    <iframe src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Washington+D.C.+white+house&amp;aq=t&amp;sll=37.0625,-95.677068&amp;sspn=60.376022,92.460937&amp;ie=UTF8&amp;hq=Washington+D.C.+white+house&amp;t=m&amp;ll=38.896984,-77.036518&amp;spn=0.006295,0.006295&amp;output=embed" width="100%" height="350" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>
                </div>
            </div>

            <div class="col-sm-4">
                <h3>We’d love to hear from you</h3>

                <div class="contact-form-wrapper clearfix background-white p30">

                    <?php echo $this->Form->create(
                        'Contacts', array(
                            'class' => 'contact-form'
                            )
                        ); 
                    ?>
                        <div class="form-group">
                            <label for="contact-form-name">Name</label>
                            <?php echo $this->Form->input(
                                'Name', array(
                                    'type' => 'text',
                                    'label' => false,
                                    'class' => 'form-control',
                                    'placeholder' => ''
                                    )
                                ); 
                            ?>  
                        </div>
                        <div class="form-group">
                            <label for="contact-form-email">E-mail</label>
                            <?php echo $this->Form->input(
                                'Email', array(
                                    'type' => 'text',
                                    'label' => false,
                                    'class' => 'form-control',
                                    'placeholder' => ''
                                    )
                                ); 
                            ?>                             
                        </div>
                        <div class="form-group">
                            <label for="contact-form-message">Message</label>
                            <?php echo $this->Form->textarea(
                                'Email', array(
                                    'type' => 'text',
                                    'label' => false,
                                    'class' => 'form-control',
                                    'rows' => '6'
                                    )
                                ); 
                            ?>                            
                        </div>
                        <?php echo $this->Form->button(
                            'Post Message', array(
                                'type' => 'submit',
                                'label' => false,
                                'class' => 'btn btn-primary pull-right'
                                )
                            ); 
                        ?>
                    <?php echo $this->Form->end(); ?>

                </div><!-- /.contact-form-wrapper -->
            </div>
        </div><!-- /.row -->
    </div><!-- /.content -->
</div><!-- /.container -->