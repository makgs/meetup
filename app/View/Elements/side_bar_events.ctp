<?php if (!empty($sideBarEvents)) { ?>
    <div class="col-md-3 col-sm-3 col-xs-12">
        <div class="sidebar">
            <div class="widget">
                <h2 class="widgettitle">
                    <?php echo $this->Html->link(
                        'Ближайшие события', 
                        array(
                            'controller' => 'events',
                            'action' => 'view_all',
                            'admin' => false,
                            'fresh'
                        ),
                        array(
                            'class' => 'my_link'
                            )
                    );?>
                </h2>

                <div class="row">
                    <? foreach ($sideBarEvents as $event) { ?>
                        <div class="col-md-12 col-sm-12 col-xs-4">
                            <div class="cards-small">
                                <div class="card-small">
                                    <div class="card-small-image">
                                        <? if (isset($event['Event']['photo'])) { ?>
                                            <? $photo = '/files/event/photo/'
                                                    . $event['Event']['photo_dir']
                                                    . '/small_'
                                                    . $event['Event']['photo'];
                                            ?>
                                        <? } else { ?>
                                            <? $photo = '/img/default_bg.png'; ?>
                                        <? } ?>
                                        <? echo $this->Html->image(
                                            $photo, 
                                            array(
                                                'url' => array(
                                                    'controller' => 'events',
                                                    'action' => 'view',
                                                    'admin' => false,
                                                    $event['Event']['id']
                                                )
                                            )
                                        ); ?>

                                    </div><!-- /.card-small-image -->

                                    <div class="card-small-content">
                                        <h3>
                                            <?php echo $this->Html->link(
                                                $this->Text->truncate($event['Event']['name'], 40), 
                                                array(
                                                    'controller' => 'events',
                                                    'action' => 'view',
                                                    'admin' => false,
                                                    $event['Event']['id']
                                                )
                                            );?>
                                        </h3>
                                        
                                        <?if (!empty($event['Event']['city'])) { ?>
                                            <h4>
                                                <?echo $this->Text->truncate($event['Event']['city'], 30);?>
                                            </h4>
                                        <? } ?>
                                    </div><!-- /.card-small-content -->
                                </div><!-- /.card-small -->
                            </div><!-- /.cards-small -->
                        </div><!-- /.col -->
                    <? }?>
                </div><!-- /.row -->
                
            </div><!-- /.widget -->
        </div><!-- /.sidebar -->
    </div><!--col-->
<? } ?>