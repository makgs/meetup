<?php
/**
 * Helper from date of end and date of start fromating
 */

App::uses('AppHelper', 'View/Helper');

class DateHelper extends AppHelper {
    
 /**
 * This method formate date based on the fact, whether the dates owned by one day or not
 *
 * @param string $fullDateStart Start date from DB
 * @param string|null $fullDateEnd End date from DB (nor required)
 *
 * @return string Html including date
 */   
    public function makeDateAndTime($fullDateStart, $fullDateEnd = null) {
        
        $fullDateStart = strtotime($fullDateStart);
        
        if (isset($fullDateEnd)) {
            $fullDateEnd   = strtotime($fullDateEnd);
        }
        
        if (abs(time() - $fullDateStart) < 60*60*24*30*8) {
            $dateFormat = 'd F';
        } else {
            $dateFormat = 'd F Y';
        }

        $timeFormat = 'G:i';
        
        $dateStart  = $this->rus_date(date($dateFormat, $fullDateStart));
        $timeStart  = $this->rus_date(date($timeFormat, $fullDateStart));
        
        if (isset($fullDateEnd)) {
            $dateEnd = $this->rus_date(date($dateFormat, $fullDateEnd));
            $timeEnd = $this->rus_date(date($timeFormat, $fullDateEnd));
        }
        
        if (!isset($fullDateEnd)) {         
            return 
                '<div class="event-time">
                    <div class="event_date">' . $dateStart . '</div>
                    <i class="fa fa-clock-o"></i>'
                    . $timeStart
                . '</div>';
        }
        
        if ($dateStart == $dateEnd) {
            return 
                '<div class="event-time">
                    <div class="event_date">' . $dateStart . '</div>
                    <i class="fa fa-clock-o"></i>'
                    . $timeStart . ' - ' . $timeEnd
                . '</div>';
        }
        
        return 
            '<div class="event-time">
                <div class="event_date">' 
                    . $dateStart . ' ' .  $timeStart
                    . ' - ' . $dateEnd . ' ' . $timeEnd
                . '</div>'
            . '</div>';
    }
    
    private function rus_date($date) {
        
        $translate = array(
             "am" => "дп",
             "pm" => "пп",
             "AM" => "ДП",
             "PM" => "ПП",
             "Monday" => "Понедельник",
             "Mon" => "Пн",
             "Tuesday" => "Вторник",
             "Tue" => "Вт",
             "Wednesday" => "Среда",
             "Wed" => "Ср",
             "Thursday" => "Четверг",
             "Thu" => "Чт",
             "Friday" => "Пятница",
             "Fri" => "Пт",
             "Saturday" => "Суббота",
             "Sat" => "Сб",
             "Sunday" => "Воскресенье",
             "Sun" => "Вс",
             "January" => "Января",
             "Jan" => "Янв",
             "February" => "Февраля",
             "Feb" => "Фев",
             "March" => "Марта",
             "Mar" => "Мар",
             "April" => "Апреля",
             "Apr" => "Апр",
             "May" => "Мая",
             "May" => "Мая",
             "June" => "Июня",
             "Jun" => "Июн",
             "July" => "Июля",
             "Jul" => "Июл",
             "August" => "Августа",
             "Aug" => "Авг",
             "September" => "Сентября",
             "Sep" => "Сен",
             "October" => "Октября",
             "Oct" => "Окт",
             "November" => "Ноября",
             "Nov" => "Ноя",
             "December" => "Декабря",
             "Dec" => "Дек",
             "st" => "ое",
             "nd" => "ое",
             "rd" => "е",
             "th" => "ое"
         );
         
        return strtr($date, $translate);
    }
}