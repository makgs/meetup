

<div class="content">
    <div class="mt-80">
        <div class="hero-image">
            <div class="hero-image-inner">
                <div class="hero-image-content display-block">
                    <div class="container">
                        <h1>Yourmeetup</h1>
                        <p>Создавайте свои события, предлагайте темы, <br>утверждайте понравившиеся, следите за реакцией аудитории.</p>
                        <?if($this->Session->read('Auth.User') === null){?>                                
                            <a href="/users/auth_vk" class="btn btn-primary btn-lg sign_vk">Войти через<i class="fa fa-vk"></i></a>
                        <?}?>
                    </div><!-- /.container -->
                </div><!-- /.hero-image-content -->

            </div><!-- /.hero-image-inner -->
        </div><!-- /.hero-image -->
    </div>

    <div class="container">
        <div class="block">
            <?if (!empty($freshEvents)) {?>
                <div class="row"><!--Start future events-->
                    <div class="col-sm-12">
                        <h1 class="page-title"><a href="/events/view_all/fresh" class="my_link">Ближайшие события </a></h1>
                        <div class="posts">
                        <?foreach ($freshEvents as $event) {?>
                            <?$event = $event['Event'];?>
                            <div class="col-sm-4">
                                <div class="post">
                                    <div class="post-image">
                                        <?if (isset($event['photo'])) {?>
                                            <? echo $this->Html->image('../files/event/photo/'.$event['id'].'/small_'.$event['photo']); ?>
                                        <?}else {?>
                                            <? echo $this->Html->image('default_bg.png'); ?>
                                        <?}?>
                                        <a class="read-more" href="/events/view/<?echo $event['id'];?>">Просмотр</a>
                                    </div><!-- /.post-image -->

                                    <div class="post-content">
                                        <h2><a href="/events/view/<?echo $event['id'];?>"><?echo $event['name'];?></a></h2>
                                        <div class="event-time">
                                            <?echo date('d.m.Y', strtotime($event['date_start']));?>
                                        </div>
                                        <p><?echo $this->Text->truncate($event['description'], 300);?></p>
                                    </div><!-- /.post-content -->
                                </div><!-- /.post -->
                            </div>
                        <?}?> 
                        </div><!-- /.posts -->
                    </div><!-- /.col-* -->
                    <div style="clear:both"></div>
                    <div class="read-more-events">
                        <?php echo $this->Html->link(
                            'Смотреть все', 
                            array(
                                'controller' => 'events',
                                'action' => 'view_all',
                                'admin' => false,
                                'fresh'
                            )
                        );?>
                    </div>
                </div><!-- /.row --><!--End future events-->
            <?}?>
            
            <?if (!empty($oldEvents)) {?>
                <div class="row"><!--Start past events-->
                    <div class="col-sm-12">
                        <h1 class="page-title"><a href="/events/view_all/old" class="my_link">Прошедшие события</a></h1>
                        <div class="posts">
                            <?foreach ($oldEvents as $event) {?>
                                <?$event = $event['Event'];?>
                                <div class="col-sm-4">
                                    <div class="post">
                                        <div class="post-image">
                                            <?if (isset($event['photo'])) {?>
                                                <? echo $this->Html->image('../files/event/photo/'.$event['id'].'/small_'.$event['photo']); ?>
                                            <?}else {?>
                                                <? echo $this->Html->image('default_bg.png'); ?>
                                            <?}?>
                                            <a class="read-more" href="/events/view/<?echo $event['id'];?>">Просмотр</a>
                                        </div><!-- /.post-image -->

                                        <div class="post-content">
                                            <h2><a href="/events/view/<?echo $event['id'];?>"><?echo $event['name'];?></a></h2>
                                            <div class="event-time">
                                                <?echo date('d.m.Y', strtotime($event['date_start']));?>
                                            </div>
                                            <p><?echo $this->Text->truncate($event['description'], 300);?></p>
                                        </div><!-- /.post-content -->
                                    </div><!-- /.post -->
                                </div>
                            <?}?> 
                        </div><!-- /.posts -->
                    </div><!-- /.col-* -->
                    <div style="clear:both"></div>
                    <div class="read-more-events">
                        <?php echo $this->Html->link(
                            'Смотреть все', 
                            array(
                                'controller' => 'events',
                                'action' => 'view_all',
                                'admin' => false,
                                'old'
                            )
                        );?>
                    </div>
                </div><!-- /.row --><!--End past events-->
            <?}?>
        </div><!-- /.block -->
    </div>
    
    <div class="block background-primary">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="box">
                        <div class="box-icon">
                            <i class="fa fa-flask"></i>
                        </div><!-- /.box-icon -->

                        <div class="box-content">
                            <h2>Создавайте свои мероприятия</h2>
                            <p>Вы можете создать свое мероприятие, назначить время его проведения и отправить ссылку участникам</p>
                        </div><!-- /.box-content -->
                    </div>
                </div><!-- /.col-sm-4 -->

                <div class="col-sm-4">
                    <div class="box">
                        <div class="box-icon">
                            <i class="fa fa-life-ring"></i>
                        </div><!-- /.box-icon -->

                        <div class="box-content">
                            <h2>Предлагайте свои темы докладов</h2>
                            <p>Давно хотите узнать о чем-то? Предложите тему доклада! А организатор ее утвердит</p>
                        </div><!-- /.box-content -->
                    </div>
                </div><!-- /.col-sm-4 -->

                <div class="col-sm-4">
                    <div class="box">
                        <div class="box-icon">
                            <i class="fa fa-thumbs-o-up"></i>
                        </div><!-- /.box-icon -->

                        <div class="box-content">
                            <h2>Поддержите важные темы</h2>
                            <p>Голосуйте за темы докладов, самое востребованное обязательно будет замечено организаторами</p>
                        </div><!-- /.box-content -->
                    </div>
                </div><!-- /.col-sm-4 -->
            </div><!-- /.row -->
        </div>
    </div><!-- /.container -->
</div>