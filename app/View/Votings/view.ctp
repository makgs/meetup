<div class="votings view">
<h2><?php echo __('Voting'); ?></h2>
	<dl>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($voting['User']['id'], array('controller' => 'users', 'action' => 'view', $voting['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Action'); ?></dt>
		<dd>
			<?php echo $this->Html->link($voting['Action']['name'], array('controller' => 'actions', 'action' => 'view', $voting['Action']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Voting'), array('action' => 'edit', $voting['Voting']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Voting'), array('action' => 'delete', $voting['Voting']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $voting['Voting']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Votings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Voting'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Actions'), array('controller' => 'actions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Action'), array('controller' => 'actions', 'action' => 'add')); ?> </li>
	</ul>
</div>
