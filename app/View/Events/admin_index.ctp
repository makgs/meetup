<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="background-white p20 mb50">
                <h2 class="page-title">Таблица событий</h2>

                <table class="table table-hover mb0">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Событие</th>
                            <th>Автор</th>
                            <th>ВК автора</th>
                            <th>Email автора</th>
                            <th>Дата начала</th>
                            <th>Дата окончания</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?foreach($events as $event) {?>
                            <?$user = $event['User'];?>
                            <?$event = $event['Event'];?>
                            <tr>
                                <th scope="row"><?echo $event['id'];?></th>
                                <td>
                                    <?php echo $this->Html->link(
                                        $this->Text->truncate($event['name'], 30),
                                        array(
                                            'controller' => 'events',
                                            'action' => 'view',
                                            'admin' => false,
                                            $event['id']
                                        )
                                    );?>
                                </td>
                                <td>
                                    <?php echo $this->Html->link(
                                        $user['first_name'] . ' ' . $user['last_name']
                                        . ' (id ' . $user['id'] . ')', 
                                        array(
                                            'controller' => 'users',
                                            'action' => 'index',
                                            'admin' => false,
                                            $user['vk_key']
                                        )
                                    );?>
                                </td>
                                <td>
                                    <a href="http://vk.com/id<? echo $user['vk_key']?>"><? echo $user['vk_key']?></a>
                                </td>
                                <td>
                                    <? echo $user['email']?>
                                </td>
                                <td><?echo date('d.m.y', strtotime($event['date_start']));?></td>
                                <td><?echo date('d.m.y', strtotime($event['date_end']));?></td>
                                <td class="delete">
                                    <?php if ($event['is_banned'] == 1) {
                                        echo $this->Form->postLink(
                                            'Разбанить',
                                            array(
                                                'controller' => 'events',
                                                'action' => 'ban',
                                                'admin' => true,
                                                $event['id']
                                            )
                                        );
                                    } else {
                                        echo $this->Form->postLink(
                                            'Забанить',
                                            array(
                                                'controller' => 'events',
                                                'action' => 'ban',
                                                'admin' => true,
                                                $event['id']
                                            )
                                        );
                                    }?>
                                </td>
                                <td class="delete">
                                    <?php echo $this->Form->postLink(
                                        'Удалить', 
                                        array(
                                            'controller' => 'events',
                                            'action' => 'delete',
                                            'admin' => true,
                                            $event['id']
                                        ),
                                        array('confirm' => 'Вы действительно хотите удалить мероприятие?')
                                    );?>
                                </td>
                            </tr>
                        <?}?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><!-- /.container-fluid -->
<? if ($this->Paginator->counter(array('format' => '%pages%')) != 1) { ?>
    <div class="pager">
        <ul>
            <?echo $this->Paginator->prev(
                '<i class="fa fa-arrow-left"></i>', 
                array(
                    'escape'=>false,
                    'tag' => 'li'
                ), 
                null, 
                array(
                    'escape'=>false, 
                    'tag' => 'li',
                    'class' => 'disabled',
                    'disabledTag' => 'a
                ')
            );?>
            <?echo $this->Paginator->numbers(
                array(
                    'separator' => '',
                    'currentTag' => 'a', 
                    'currentClass' => 'active',
                    'tag' => 'li','first' => 1
                )
            );?>
            <?echo $this->Paginator->next(
                '<i class="fa fa-arrow-right"></i>', 
                array(
                    'escape'=>false, 
                    'tag' => 'li',
                    'currentClass' => 'disabled'
                ), 
                null, 
                array(
                    'escape'=>false,
                    'tag' => 'li',
                    'class' => 'disabled',
                    'disabledTag' => 'a'
                )
            );?>
        </ul>
    </div><!-- /.pagination -->
<? } ?>