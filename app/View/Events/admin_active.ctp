<div class="col-sm-12">
    <div class="page-title">
        <h1 style="text-align: center;"><?if (!empty($events)) {?>Список ваших мероприятий<?} else {?>У вас пока нет мероприятий<? }?></h1>
    </div><!-- /.page-title -->
    <span id="back-top">
        <i class="fa fa-arrow-circle-up"></i>
    </span>
    <div class="cards-row">
        <?foreach($events as $event){?>
            <?$event = $event['Event'];?>
            <div class="card-row">
                <div class="card-row-inner">
                <?if ($event['photo'] != NULL) {?>
                    <?if ($event['is_banned'] == 1) {?>
                        <div class="card-row-image" data-background-image="/files/event/photo/<? echo $event['photo_dir'] . '/small_' . $event['photo'] ?>">
                            <div class="no_public"></div>
                            <span class="ban">
                                Забанено
                            </span>
                        </div>     
                    <?} else {?>
                        <? if ($event['is_public'] == 1) {?>
                        <div class="card-row-image" data-background-image="/files/event/photo/<? echo $event['photo_dir'] . '/small_' . $event['photo'] ?>"></div>
                        <? } elseif ($event['is_public'] == 0) {?>
                        <div class="card-row-image" data-background-image="/files/event/photo/<? echo $event['photo_dir'] . '/small_' . $event['photo'] ?>">
                            <div class="no_public"></div>
                                <span class="public_button">
                                    <?echo $this->Form->postLink(
                                        'Опубликовать',  
                                        array(
                                            'controller' => 'events',
                                            'action' => 'publish',
                                            'admin' => true,
                                            $event['id']
                                        )
                                    ); ?>
                                </span>
                        </div>
                        <? }?>
                    <?}?>
                <?} else {?>
                    <?if ($event['is_banned'] == 1) {?>
                        <div class="card-row-image" data-background-image="/img/default_bg.png">
                            <div class="no_public"></div>
                            <span class="ban">
                                Забанено
                            </span>
                        </div> 
                    <?} else {?>
                        <? if ($event['is_public'] == 1) {?>
                            <div class="card-row-image" data-background-image="/img/default_bg.png"></div>
                        <? } elseif ($event['is_public'] == 0) {?>
                            <div class="card-row-image" data-background-image="/img/default_bg.png">
                                <div class="no_public"></div>
                                <span class="public_button">
                                    <?echo $this->Form->postLink(
                                        'Опубликовать',  
                                        array(
                                            'controller' => 'events',
                                            'action' => 'publish',
                                            'admin' => true,
                                            $event['id']
                                        )
                                    ); ?>
                                </span>
                            </div>
                        <?}?>
                    <?}?>
                <?}?>
            <!-- /.card-row-image -->

                    <div class="card-row-body">
                        <h2 class="card-row-title">
                            <?php echo $this->Html->link(
                                $event['name'], 
                                array(
                                    'controller' => 'events',
                                    'action' => 'view',
                                    'admin' => false,
                                    $event['id']
                                )
                            );?>
                        </h2>
                        <?if ($event['date_start'] != null) {?>
                            <? echo $this->Date->makeDateAndTime(
                                $event['date_start'], 
                                $event['date_end']
                            );?>
                        <?}?>
                        <div class="card-row-content"><p><?php echo nl2br($this->Text->truncate($event['description'], 300))?></p></div><!-- /.card-row-content -->
                        <?php
                            if ($event['is_public'] == 1) {

                                echo $this->Form->postLink(
                                'Скрыть /',
                                array(
                                    'controller' => 'events',
                                    'action' => 'publish',
                                    'admin' => true,
                                    $event['id']
                                    )
                                );
                            }
                            ?>
                        <?php echo $this->Html->link(
                            'Изменить', 
                            array(
                                'controller' => 'events',
                                'action' => 'edit',
                                'admin' => true,
                                $event['id']
                            )
                        );?> /
                        <?php echo $this->Form->postLink(
                            'Удалить', 
                            array(
                                'controller' => 'events',
                                'action' => 'delete',
                                'admin' => true,
                                $event['id']
                            ),
                            array('confirm' => 'Вы действительно хотите удалить мероприятие?')
                        );?>
                    </div><!-- /.card-row-body -->
                    <?if ($event['city'] != null && $event['address'] != null) {?>
                        
                        <div class="card-row-properties">
                            <dl>                               
                                <dd>Местоположение &nbsp;</dd><dt><?php echo $event['city']?> / <?php echo $event['address']?></dt>
                            </dl>
                        </div><!-- /.card-row-properties -->
                    <?}?>
                </div><!-- /.card-row-inner -->
            </div><!-- /.card-row -->
        <?}?>
    </div><!-- /.cards-row -->
    <? if ($this->Paginator->counter(array('format' => '%pages%')) != 1) { ?>
        <div class="pager">
            <ul>
                <?echo $this->Paginator->prev(
                    '<i class="fa fa-arrow-left"></i>', 
                    array(
                        'escape'=>false,
                        'tag' => 'li'
                    ), 
                    null, 
                    array(
                        'escape'=>false, 
                        'tag' => 'li',
                        'class' => 'disabled',
                        'disabledTag' => 'a
                    ')
                );?>
                <?echo $this->Paginator->numbers(
                    array(
                        'separator' => '',
                        'currentTag' => 'a', 
                        'currentClass' => 'active',
                        'tag' => 'li','first' => 1
                    )
                );?>
                <?echo $this->Paginator->next(
                    '<i class="fa fa-arrow-right"></i>', 
                    array(
                        'escape'=>false, 
                        'tag' => 'li',
                        'currentClass' => 'disabled'
                    ), 
                    null, 
                    array(
                        'escape'=>false,
                        'tag' => 'li',
                        'class' => 'disabled',
                        'disabledTag' => 'a'
                    )
                );?>
            </ul>
        </div><!-- /.pagination -->
    <? } ?>
</div><!-- /.col-* -->