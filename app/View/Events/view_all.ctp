<div class="main-inner_custom negative-margin-top"> 
    <div class="container">
        <div class="content">
            <span id="back-top">
                <i class="fa fa-arrow-circle-up"></i>
            </span>
            <h2 class="page-title">
                <?if (empty($Events)) {?>
                    Здесь пока нет событий
                <?} elseif ($this->request->pass[0] == 'old') {?>
                    Прошедшие события
                <?} elseif ($this->request->pass[0] == 'fresh') {?>
                    Ближайшие события
                <?} else {?>
                    Все события
                <?}?>
            </h2><!-- /.page-title -->
            <div class="cards-row">
                <?foreach ($Events as $event) {?>
                    <?$event = $event['Event'];?>
                    <div class="card-row">
                        <div class="card-row-inner">
                            <?if (isset($event['photo'])) {?>
                                <div class="card-row-image" data-background-image="/files/event/photo/<?echo $event['id'].'/'.$event['photo'];?>"></div><!-- /.card-row-image -->
                            <?}else {?>
                                <div class="card-row-image" data-background-image="/img/default_bg.png"></div><!-- /.card-row-image -->
                            <?}?>
                            <div class="card-row-body">
                                <h2 class="card-row-title"><a href="/events/view/<?echo $event['id'];?>"><?echo $event['name'];?></a></h2>
                                <div class="event_date">
                                    <?if ($event['date_start'] != null) {?>
                                        <? echo $this->Date->makeDateAndTime(
                                            $event['date_start'], 
                                            $event['date_end']
                                        );?>
                                    <?}?>
                                </div>
                                <div class="card-row-content"><p><?echo $this->Text->truncate($event['description'], 500);?></p></div><!-- /.card-row-content -->
                            </div><!-- /.card-row-body -->
                        </div><!-- /.card-row-inner -->
                    </div><!-- /.card-row -->
                <?}?>
                
            </div><!-- /.cards-row -->

            <? if ($this->Paginator->counter(array('format' => '%pages%')) != 1) { ?>
                <div class="pager">
                    <ul>
                        <?echo $this->Paginator->prev(
                            '<i class="fa fa-arrow-left"></i>', 
                            array(
                                'escape'=>false,
                                'tag' => 'li'
                            ), 
                            null, 
                            array(
                                'escape'=>false, 
                                'tag' => 'li',
                                'class' => 'disabled',
                                'disabledTag' => 'a
                            ')
                        );?>
                        <?echo $this->Paginator->numbers(
                            array(
                                'separator' => '',
                                'currentTag' => 'a', 
                                'currentClass' => 'active',
                                'tag' => 'li','first' => 1
                            )
                        );?>
                        <?echo $this->Paginator->next(
                            '<i class="fa fa-arrow-right"></i>', 
                            array(
                                'escape'=>false, 
                                'tag' => 'li',
                                'currentClass' => 'disabled'
                            ), 
                            null, 
                            array(
                                'escape'=>false,
                                'tag' => 'li',
                                'class' => 'disabled',
                                'disabledTag' => 'a'
                            )
                        );?>
                    </ul>
                </div><!-- /.pagination -->
            <? } ?>
        </div><!-- /.content -->
    </div><!-- /.container -->
</div>   