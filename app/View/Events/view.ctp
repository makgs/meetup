<span id="back-top">
    <i class="fa fa-arrow-circle-up"></i>
</span>
<div class="mt-80 mb80 custom-padding">
    <?if (isset($event['Event']['photo'])) {?>
        <div class="detail-banner custom-background" style="background-image: url(/files/event/photo/<? echo $event['Event']['photo_dir'] . '/big_' . $event['Event']['photo'] ?>);">
    <?} else {?>
        <div class="detail-banner custom-background" style="background-image: url(/img/tmp/detail-banner-1.jpg);">
    <?}?>
        <div class="container">
            <div class="detail-banner-left">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="detail-title">
                            <?php echo $event['Event']['name']; ?>
                        </h2>
                        <?if (!empty($event['Event']['city']) || !empty($event['Event']['address'])) {?>
                            <div class="detail-banner-address">
                                <i class="fa fa-map-o"></i> 
                                <?php echo $event['Event']['city']; ?>
                                <?php echo $event['Event']['address']; ?>
                            </div><!-- /.detail-banner-address -->
                        <?}?>
                        
                        <div class="detail-banner-rating subscriptions">
                            <i class="fa fa-star"></i>
                            <span>
                                <strong id="followers_count"><?echo $event['Event']['subscription_count']; ?></strong>
                                добавили в избранное
                            </span>
                        </div>
                        <?if($event['Event']['is_banned'] == 0) { ?>
                            <? if (isset($user_id)) { ?>
                                <? if (!$isSubscription) { ?>
                                    <div class="detail-banner-btn bookmark" id="subscribe" data-event-id="<? echo $event['Event']['id']; ?>">
                                        <i class="fa fa-bookmark-o"></i> <span data-toggle="Убрать из избранных">Добавить в избранное</span>
                                    </div>
                                <? } else { ?>
                                    <div class="detail-banner-btn bookmark marked" id="subscribe" data-event-id="<? echo $event['Event']['id']; ?>">
                                        <i class="fa fa-bookmark-o"></i> <span data-toggle="Добавить в избранное">Убрать из избранных</span>
                                    </div>
                                <? } ?>
                            <? } ?>
                        <?}?>
                    </div><!-- /.detail-banner-left -->
                </div>
            </div>
        </div><!-- /.container -->
    </div><!-- /.detail-banner -->
</div>

<div class="container">
    <div class="row detail-content">
        <?if($event['Event']['is_banned'] == 1) { ?>
            <div class="alert alert-danger" role="alert">
                <strong>Внимание! </strong>Ваше событие забанено!<br>
                О причинах бана Вы можете узнать из сообщения, отправленного на Вашу страницу в vk.com или почту
            </div>
        <?}elseif ($event['Event']['is_public'] == 0) { ?>
            <div class="alert alert-danger" role="alert">
                <strong>Внимание!</strong> Это событие неопубликовано! Оно видно только вам </br>
                <?php echo $this->Form->postLink(
                    'Опубликовать',  
                    array(
                        'controller' => 'events',
                        'action' => 'publish',
                        'admin' => true,
                        $event['Event']['id']
                        )
                    );                                

                ?> /
                <?php echo $this->Html->link(
                    'Изменить', 
                    array(
                        'controller' => 'events',
                        'action' => 'edit',
                        'admin' => true,
                        $event['Event']['id']
                    )
                );?> /
                <?php echo $this->Form->postLink(
                    'Удалить', 
                    array(
                        'controller' => 'events',
                        'action' => 'delete',
                        'admin' => true,
                        $event['Event']['id']
                    ),
                    array('confirm' => 'Вы действительно хотите удалить мероприятие?')
                );?>
            </div>
        <? }?>
        <div class="col-md-9 col-sm-9 col-xs-12" id="data">
            <h1><?php echo $event['Event']['name']; ?></h1>
            <? if (isset($event['Event']['date_start'])) {?>
                <? echo $this->Date->makeDateAndTime(
                    $event['Event']['date_start'], 
                    $event['Event']['date_end']
                );?>
            <?}?>
            <? if (isset($event['Event']['description'])) {?>                
                <div class="row">
                    <div class="col-md-10 col-sm-10">
                        <div class="detail-description">
                            <?php echo nl2br($event['Event']['description']); ?>                            
                        </div>
                        <h5 class="user_vk">Автор: <?php echo $this->Html->link(
                            $user_info, 
                                array(
                                    'controller' => 'users',
                                    'action' => 'index/' . $user_key
                                )
                            );?>
                        </h5>
                    </div>
                </div>
            <?}?>

            <div class="row form-padding-bottom">              
                <div class="form-group col-md-10 col-md-push-0 col-sm-11 col-sm-push-0 col-xs-10 col-xs-push-1">
                    <?if (!empty($approvedActions)) {?>
                        <h2>Утвержденные темы</h2>
                        <!--Start organizers accordion-->
                        <div id="accordion" class="panel-group">
                            <?foreach($approvedActions as $key => $action){?>
                                <?$num = $key + 1;?>
                                <div class="panel panel-success">
                                  <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-9 col-sm-8 col-xs-12">
                                            <span class="panel-title">
                                                <a href="#collapse-<?php echo $num?>" data-parent="#accordion" data-toggle="collapse"><?php echo $action['Action']['name']?></a>
                                            </span>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <span class="like_theme"><i class="fa fa-heart like_heart <?php if (isset($user_id)) {if ($approvedActions[$key]['Action']['is_liked'] == false) { echo '';} else { echo 'active';} } else {echo 'dont_reg';} ?>"></i>
                                                <i class="fa fa-thumbs-o-up like_ft_thumbs" data-toggle="modal" data-target="">
                                                    <span id="<?php echo $num; ?>" class="all_likes" data-fixlike="<?php echo $action['Action']['id'];?>">
                                                        <?php echo $action['Action']['voting_count'];?>
                                                    </span>
                                                </i>
                                            </span>
                                        </div>
                                    </div>
                                  </div>
                                  <div id="collapse-<?php echo $num;?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p><?php if ($action['Action']['description'] == '') {echo 'У темы нет описания';} else {echo nl2br($action['Action']['description']);}  ?></p>
                                        <?if ($eventOwnedByUser) {?>
                                            <div class="actions-buttons">
                                                <?/*move to «potential»*/?>
                                                <?php echo $this->Form->postLink(
                                                    'Отклонить', 
                                                    array(
                                                        'controller' => 'actions',
                                                        'action' => 'unapprove',
                                                        'admin' => true,
                                                        $action['Action']['id']
                                                    ),
                                                    array(
                                                        'title' => 'Сделать тему обсуждаемой'
                                                    )
                                                );?>
                                                &nbsp;/&nbsp;
                                                <?/*remove*/?>
                                                <?php echo $this->Form->postLink(
                                                    'Удалить', 
                                                    array(
                                                        'controller' => 'actions',
                                                        'action' => 'delete',
                                                        'admin' => true,
                                                        $action['Action']['id']
                                                    ),
                                                    array(
                                                        'title' => 'Удалить тему',
                                                        'confirm' => 'Вы действительно хотите удалить тему?'
                                                    )
                                                );?>
                                            </div>
                                            <div style="clear:both"></div>
                                        <?}?>
                                    </div>
                                  </div>
                                </div>
                            <?}?>
                        </div><!--End users accordion-->
                    <?}?>   
                    
                    <?if (!empty($unApprovedActions)) {?>
                        <h2>Возможные темы</h2>

                                <!--Start users accordion-->
                        <div id="accordion-1" class="panel-group">
                            <?foreach($unApprovedActions as $key => $usersAction){?>
                                <? if (isset($num)) {
                                    $num = $num + 1;
                                } else {
                                    $num = 1;
                                }
                                ?>
                                <div class="panel panel-success">
                                  <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-9 col-sm-8 col-xs-12">
                                            <span class="panel-title">
                                                <a href="#collapse-<?php echo $num?>" data-parent="#accordion-1" data-toggle="collapse"><?php echo $usersAction['Action']['name']?></a>
                                            </span>
                                        </div>
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <span class="like_theme"><i class="fa fa-heart like_heart <?php if (isset($user_id)) {if ($unApprovedActions[$key]['Action']['is_liked'] == false) {echo '';} else {echo 'active';}} else { echo 'dont_reg';} ?>"></i>
                                                <i class="fa fa-thumbs-o-up like_dt_thumbs" data-toggle="modal" data-target="">
                                                    <span id="<?php echo $num; ?>" class="all_likes" data-like="<?php echo $usersAction['Action']['id']; ?>">
                                                        <?php echo $usersAction['Action']['voting_count'];?>
                                                    </span>
                                                </i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div id="collapse-<?php echo $num?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p><?php if ($usersAction['Action']['description'] == '') {echo 'У темы нет описания';} else {echo nl2br($usersAction['Action']['description']); } ?></p>
                                        <?if ($eventOwnedByUser) {?>
                                            <div class="actions-buttons">
                                                <?/*approve*/?>
                                                <?php echo $this->Form->postLink(
                                                    'Утвердить', 
                                                    array(
                                                        'controller' => 'actions',
                                                        'action' => 'approve',
                                                        'admin' => true,
                                                        $usersAction['Action']['id']
                                                    ),
                                                    array(
                                                        'title' => 'Сделать тему утвержденной'
                                                    )
                                                );?>
                                                &nbsp;/&nbsp;
                                                <?/*remove*/?>
                                                <?php echo $this->Form->postLink(
                                                    'Удалить', 
                                                    array(
                                                        'controller' => 'actions',
                                                        'action' => 'delete',
                                                        'admin' => true,
                                                        $usersAction['Action']['id']
                                                    ),
                                                    array(
                                                        'title' => 'Удалить тему',
                                                        'confirm' => 'Вы действительно хотите удалить тему?'
                                                    )
                                                );?>
                                            </div>
                                            <div style="clear:both"></div>
                                        <?}?>
                                    </div>
                                </div>
                            </div>
                            <?}?>

                        </div><!--End users accordion-->
                    <?}?>

                    <div class="modal fade" id="modal-reg">
                        <div class="modal-dialog modal-xs">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button class="close" type="button" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Внимание!</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Вы не зарегистрированы</p>
                                </div>
                                <div class="modal-footer">
                                    <?php echo $this->Html->link(
                                        'Авторизация VK', 
                                        array(
                                            'controller' => 'users',
                                            'action' => 'auth_vk',
                                            'admin' => false
                                        )
                                    );?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (isset($user_id)) {?>
                    
                        <h2>Предложите свою тему</h2>
                        
                        <?if ( isset($validationErrors['name'][0]) && $validationErrors['name'][0] == 'notBlank') {?>
                        
                            <div class="alert alert-danger">
                              <strong>Ошибка!</strong> Не заполнено название
                            </div>
                        <?}?>
                        
                        <?php echo $this->Form->create('Action', array('action' => 'add'));?>
                        <?php echo $this->Form->input(
                            'name', 
                            array(
                                'div' => false, 
                                'label' => false, 
                                'class' => 'user_Dform form-control', 
                                'placeholder' => 'Название темы'
                            )
                        );?>
                        <?php echo $this->Form->input(
                            'description', 
                            array(
                                'div' => false, 
                                'label' => false, 
                                'class' => 'user_Dform form-control', 
                                'placeholder' => 'Описание темы',
                                'rows' => 5
                            )
                        );?>
                        <?php echo $this->Form->input(
                            'event_id', 
                            array(
                                'type' => 'hidden',
                                'value' => $event['Event']['id']
                            )
                        );?>
                        <?php $options = array(
                            'label' => 'Отправить',
                            'div' => false,
                            'class' => 'btn btn-primary user_Dform'
                        ); ?>
                        <?php echo $this->Form->end($options); ?>
                    <?} else {?>
                        <p>
                            <?php echo $this->Html->link(
                                'Авторизуйтесь', 
                                array(
                                    'controller' => 'users',
                                    'action' => 'auth_vk',
                                    'admin' => false
                                )
                            );?>,
                            чтобы голосовать за темы и предлагать свои
                        </p>
                        
                    <?}?>
                </div><!-- /.col-sm-6 -->
            </div><!-- /.row -->

            <div class="row">
                <div class="vk-comments col-md-10 col-md-push-0 col-sm-11 col-sm-push-0 col-xs-10 col-xs-push-1">
                    <div id="vk_comments"></div>
                </div>
            </div>
            <script type="text/javascript">
                VK.init({apiId: 5112153, onlyWidgets: true});
                VK.Widgets.Comments('vk_comments', {limit: 5, autoPublish: 0});
            </script>
            
        </div><!-- /.col-* -->
        <?echo $this->element('side_bar_events', $sideBarEvents);?>
    </div><!-- /.row -->
</div><!-- /.container -->

