<div class="col-sm-12">
    <div class="page-title">
        <h1 style="text-align: center;"><?if (!empty($events)) {?>Ваши избранные события<?} else {?>Нет избранных событий<? }?></h1>
    </div><!-- /.page-title -->
    <span id="back-top">
        <i class="fa fa-arrow-circle-up"></i>
    </span>
    <div class="cards-row">
        <?foreach($events as $event){?>
            <?$event = $event['Event'];?>
            <div class="card-row">
                <div class="card-row-inner">
                <?if ($event['photo'] != NULL) {?>
                    <div class="card-row-image" data-background-image="/files/event/photo/<? echo $event['photo_dir'] . '/small_' . $event['photo'] ?>"></div>
                <?} else {?>
                    <div class="card-row-image" data-background-image="/img/tmp/product-1.jpg"></div>
                <?}?>
            <!-- /.card-row-image -->

                    <div class="card-row-body">
                        <h2 class="card-row-title">
                            <?php echo $this->Html->link(
                                $event['name'], 
                                array(
                                    'controller' => 'events',
                                    'action' => 'view',
                                    'admin' => false,
                                    $event['id']
                                )
                            );?>
                        </h2>
                        <?if ($event['date_start'] != null) {?>
                            <? echo $this->Date->makeDateAndTime(
                                $event['date_start'], 
                                $event['date_end']
                            );?>
                        <?}?>
                        <div class="card-row-content"><p><?php echo nl2br($this->Text->truncate($event['description'], 300))?></p></div><!-- /.card-row-content -->
                        <div class="detail-banner-btn bookmark marked user-subscribtions-unsubscribe" id="subscribe" data-event-id="<? echo $event['id']; ?>">
                            <i class="fa fa-bookmark-o"></i> <span data-toggle="Добавить в избранное">Убрать из избранных</span>
                        </div>
                    </div><!-- /.card-row-body -->
                    <?if ($event['city'] != null && $event['address'] != null) {?>
                        
                        <div class="card-row-properties">
                            <dl>                               
                                <dd>Местоположение &nbsp;</dd><dt><?php echo $event['city']?> / <?php echo $event['address']?></dt>
                            </dl>
                        </div><!-- /.card-row-properties -->
                    <?}?>
                </div><!-- /.card-row-inner -->
            </div><!-- /.card-row -->
        <?}?>
    </div><!-- /.cards-row -->
    <? if ($this->Paginator->counter(array('format' => '%pages%')) != 1) { ?>
        <div class="pager">
            <ul>
                <?echo $this->Paginator->prev(
                    '<i class="fa fa-arrow-left"></i>', 
                    array(
                        'escape'=>false,
                        'tag' => 'li'
                    ), 
                    null, 
                    array(
                        'escape'=>false, 
                        'tag' => 'li',
                        'class' => 'disabled',
                        'disabledTag' => 'a
                    ')
                );?>
                <?echo $this->Paginator->numbers(
                    array(
                        'separator' => '',
                        'currentTag' => 'a', 
                        'currentClass' => 'active',
                        'tag' => 'li','first' => 1
                    )
                );?>
                <?echo $this->Paginator->next(
                    '<i class="fa fa-arrow-right"></i>', 
                    array(
                        'escape'=>false, 
                        'tag' => 'li',
                        'currentClass' => 'disabled'
                    ), 
                    null, 
                    array(
                        'escape'=>false,
                        'tag' => 'li',
                        'class' => 'disabled',
                        'disabledTag' => 'a'
                    )
                );?>
            </ul>
        </div><!-- /.pagination -->
    <? } ?>
</div><!-- /.col-* -->