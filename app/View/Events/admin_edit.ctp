<?php echo $this->Form->create('Event', array('type' => 'file')); ?>
<div class="row">
    <span id="back-top">
        <i class="fa fa-arrow-circle-up"></i>
    </span>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-md-6 col-md-push-3"> <!-- Start info event -->

                <div class="page-title add_forms">
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <h1 class="top_header_form">Редактирование события</h1>   
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <button type="submit" class="btn btn-success button_top_form"><i class="fa fa-check-square-o"></i> Сохранить</button>  
                        </div>
                    </div>
                </div><!-- /.page-title -->
                <div class="background-white p20 mb50">
                    <h2 class="page-title">Информация о событии</h2>

                    <div class="form-group">
                        <label for="exampleInputText1">Название события <span class="required">*</span></label>
                        <?php echo $this->Form->input(
                            'name', array(
                                'label' => false,
                                'class' => 'form-control',
                                'id' => 'exampleInputText1', 
                                'placeholder' => 'Название события',
                                'required' => true
                                )
                            ); 
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputText1">Дата начала <span class="required">*</span></label>
                        <? if (isset($event['Event']['date_start'])) { ?>
                            <?php echo $this->Form->input(
                                'date_start', array(
                                    'label' => false,
                                    'type' => 'text',
                                    'class' => 'form-control datetimepicker',
                                    'id' => 'exampleInputText2', 
                                    'placeholder' => 'Дата начала события',
                                    'value' => date('Y-m-d H:i', strtotime($event['Event']['date_start'])),
                                    'required' => true
                                )
                            ); 
                            ?>
                        <?} else {?>
                            <?php echo $this->Form->input(
                                'date_start', array(
                                    'label' => false,
                                    'type' => 'text',
                                    'class' => 'form-control datetimepicker',
                                    'id' => 'exampleInputText2', 
                                    'placeholder' => 'Дата начала события',
                                    'required' => false
                                )
                            ); 
                            ?>
                        <?} ?>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputText1">Дата завершения </label>
                        <? if (isset($event['Event']['date_end'])) { ?>
                            <?php echo $this->Form->input(
                                'date_end', array(
                                    'label' => false,
                                    'type' => 'text',
                                    'class' => 'form-control datetimepicker',
                                    'id' => 'exampleInputText3', 
                                    'placeholder' => 'Дата завершения события',
                                    'value' => date('Y-m-d H:i', strtotime($event['Event']['date_end'])),
                                    'required' => false
                                )
                            ); 
                            ?>
                        <?} else { ?>
                            <?php echo $this->Form->input(
                                'date_end', array(
                                    'label' => false,
                                    'type' => 'text',
                                    'class' => 'form-control datetimepicker',
                                    'id' => 'exampleInputText3', 
                                    'placeholder' => 'Дата завершения события',
                                    'required' => false
                                )
                            ); 
                            ?>
                        <?} ?>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputText1">Ваш город </label>
                        <?php echo $this->Form->input(
                            'city', array(
                                'label' => false,
                                'class' => 'form-control',
                                'id' => 'exampleInputText4', 
                                'placeholder' => 'Ваш город'
                                )
                            ); 
                        ?>
                    </div><!-- /.form-group -->

                    <div class="form-group">
                        <label for="exampleInputText1">Адрес </label>
                        <?php echo $this->Form->input(
                            'address', array(
                                'label' => false,
                                'class' => 'form-control',
                                'id' => 'exampleInputText5', 
                                'placeholder' => 'Адрес'
                                )
                            ); 
                        ?>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputTextarea">Описание события <span class="required">*</span></label>
                        <?php echo $this->Form->input(
                            'description', array(
                                'label' => false,
                                'type' => 'textarea',
                                'class' => 'form-control',
                                'id' => 'exampleInputText6', 
                                'placeholder' => 'Пара строк о событии',
                                'required' => true
                                )
                            ); 
                        ?>
                    </div>

                    <div class="form-group event-image">
                        <label for="exampleInputFile"></label>
                        <?php echo $this->Form->input('Event.photo', array('type' => 'file', 'label' => false)); ?>
                        <?php echo $this->Form->input('Event.photo_dir', array('type' => 'hidden')); ?>
                        <?php if (isset($event['Event']['photo'])) { ?>
                            <img src="/files/event/photo/<? echo $event['Event']['photo_dir'] . '/small_' . $event['Event']['photo'] ?>">
                            <?php echo $this->Form->input('Event.photo.remove', array('type' => 'checkbox', 'label' => 'Удалить изображение'));?>
                        <? } ?>
                    </div>             
                </div>
            </div><!-- End info event-->
        </div>

        <div class="row"> <!-- Start fixed action -->
            <div class="col-md-6 col-md-push-3">
                <div class="background-white p20 mb80">
                    <h2 class="page-title add_fix">Утвержденные темы</h2>

                    <div class="fixed_theme">
                        <div class="ft_inputs" id="0" style="display: none"></div>
                        
                        <?foreach($event['Action'] as $key => $action){?>
                                <?if ($action['is_approved'] == 1) {?>
                                    <div id="<?php echo $action['id']?>" class="ft_inputs" style="display: block;">
                                        <input type="text" class="ft_input form-control" required="required" name="change_ft[name][<?php echo $action['id'] ?>]" placeholder="Name theme" value="<?php echo $action['name'] ?>">
                                        <textarea class="ft_input form-control" rows="5" name="change_ft[text][<?php echo $action['id'] ?>]" placeholder="Desription theme"><?php echo $action['description'] ?></textarea>
                                        <span class="remove_fixed_theme btn btn-danger">Удалить тему</span>
                                    </div>
                                <?}?>
                        <?}?>
                        
                        <div class="ft_inputs" id="0" style="display: none"></div>
                        
                    </div>
                    <div class="col-md-8 col-md-push-2 col-sm-6 col-sm-push-3 col-xs-12">
                        <div class="control_theme">
                            <ul>
                                <li><span id="add_fixed_theme" class="btn btn-primary">Добавить тему</span></li>
                                <li><span class="btn btn-danger" data-toggle="modal" data-target="#modal-fix">Очистить</span></li>
                            </ul>
                        </div>
                    </div>    
                </div>
            </div>

            <div class="modal fade" id="modal-fix">
                <div class="modal-dialog modal-xs">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Внимание!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Вы уверены, что хотите удалить все темы?</p>
                        </div>
                        <div class="modal-footer">
                            <span class="btn btn-danger" type="button" data-dismiss="modal" id="reset_fixed_theme">Да</span>
                            <span class="btn btn-danger" type="button" data-dismiss="modal">Нет</span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End fixed action -->

        <div class="row"><!-- Start discussed action -->
            <div class="col-md-6 col-md-push-3">
                <div class="background-white p20 mb80">
                    <h2 class="page-title add_disc">Возможные темы</h2>

                    <div class="discussed_theme">
                        <div class="dt_inputs" id="0" style="display: none"></div>
                        
                        <?foreach($event['Action'] as $key => $action){?>
                                <?if ($action['is_approved'] == 0) {?>
                                    <div id="<?php echo $action['id'] ?>" class="dt_inputs" style="display: block;">
                                        <input type="text" class="dt_input form-control" required="required" name="change_dt[name][<?php echo $action['id'] ?>]" placeholder="Name theme" value="<?php echo $action['name'] ?>">
                                        <textarea class="dt_input form-control" rows="5" name="change_dt[text][<?php echo $action['id'] ?>]" placeholder="Desription theme"><?php echo $action['description'] ?></textarea>
                                        <span class="remove_theme btn btn-danger">Удалить тему</span>
                                    </div>
                                <?}?>
                        <?}?>  
                        
                        <div class="dt_inputs" id="0" style="display: none"></div>
                        
                    </div>
                    <div class="col-md-8 col-md-push-2 col-sm-6 col-sm-push-3 col-xs-12">
                        <div class="control_theme">
                            <ul>
                                <li><span id="add_theme" class="btn btn-primary">Добавить тему</span></li>
                                <li><span class="btn btn-danger" data-toggle="modal" data-target="#modal-desc">Очистить</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-desc">
                <div class="modal-dialog modal-xs">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Внимание!</h4>
                        </div>
                        <div class="modal-body">
                            <p>Вы уверены, что хотите удалить все темы?</p>
                        </div>
                        <div class="modal-footer">
                            <span class="btn btn-danger" type="button" data-dismiss="modal" id="reset_theme">Да</span>
                            <span class="btn btn-danger" type="button" data-dismiss="modal">Нет</span>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- End discussed action -->
        <div class="bottom_form">
            <button type="submit" class="btn btn-success button_bottom_form"><i class="fa fa-check-square-o"></i> Сохранить</button>
        </div>
     </div>
</div>
 <?php echo $this->Form->end(); ?>