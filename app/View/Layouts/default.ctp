<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0.8, minimum-scale=0.5, maximum-scale=1">
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php   
        echo $this->Html->css('font-awesome.min');
        echo $this->Html->css('bootstrap-select.min');
        echo $this->Html->css('superlist');
        echo $this->Html->css('my');    
        echo $this->Html->css('http://fonts.googleapis.com/css?family=Nunito:300,400,700');
		echo $this->fetch('meta');
		echo $this->fetch('css');
	?>
    <script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
</head>
<body>
<div class="page-wrapper">
    
    <header class="header">
    <div class="header-wrapper">
        <div class="container">
            <div class="header-inner">
                <div class="header-logo">
                    <a href="/">
                        <i class="fa fa-users"></i>
                        <span>Yourmeetup</span>
                    </a>
                </div><!-- /.header-logo -->

                <div class="header-content">
                    <div class="header-bottom">
                        <div class="header_right auth_vk">
                            <?if($this->Session->read('Auth.User') === null){?>                                
                                <!--<a href="events/view_all" class="btn btn-primary btn-lg sign_vk">События<i class="fa fa-comments-o"></i></i></a>-->
                                <a href="/users/auth_vk" class="btn btn-primary btn-lg sign_vk">Войти через<i class="fa fa-vk"></i></a>
                            <?}else{?>
                        </div>
                        <ul class="header-nav-primary nav nav-pills collapse navbar-collapse">
                            <li>
                                <?php echo $this->Html->link(
                                    'Профиль', 
                                    array(
                                        'controller' => 'users',
                                        'action' => 'index',
                                        'admin' => false,
                                        $this->Session->read('Auth.User.vk_key')
                                    )
                                );?>
                            </li>
                            <li>
                                <?php echo $this->Html->link(
                                    'Создать событие', 
                                    array(
                                        'controller' => 'events',
                                        'action' => 'add',
                                        'admin' => true
                                    )
                                );?>
                            </li>
                            <li>
                                <?php echo $this->Html->link(
                                    'Управление событиями', 
                                    array(
                                        'controller' => 'events',
                                        'action' => 'active',
                                        'admin' => true
                                    )
                                );?>  
                            </li>
                            <li>
                                <?php echo $this->Html->link(
                                    'Избранное', 
                                    array(
                                        'controller' => 'events',
                                        'action' => 'subscriptions',
                                        'admin' => true
                                    )
                                );?>  
                            </li>                              
                            <li>
                                <?php echo $this->Html->link(
                                    'Выйти', 
                                    array(
                                        'controller' => 'users',
                                        'action' => 'logout',
                                        'admin' => false
                                    )
                                );?>  
                            </li> 
                        </ul>
                        <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav-primary">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <?}?>
                    </div><!-- /.header-bottom -->
                </div><!-- /.header-content -->
            </div><!-- /.header-inner -->
        </div><!-- /.container -->
    </div><!-- /.header-wrapper -->
</header><!-- /.header -->




    <div class="main">
        <div class="main-inner">

                <?php echo $this->fetch('content'); ?>
                
        </div><!-- /.main-inner -->
    </div><!-- /.main -->

    
    
    
    
    <footer class="footer">

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-push-8">
                    <div class="footer-bottom-right">
                        На yourmeetup.com вы можете заявить о своем мероприятии, а посетители поддержат темы ваших докладов или предложат свои. &copy; 2015
                    </div><!-- /.footer-bottom-right -->
                </div>
            </div>
        </div><!-- /.container -->
    </div>
    </footer><!-- /.footer -->
<div id="back_top_panel">
    <span><i class="fa fa-arrow-circle-up"></i>Наверх</span>
</div>
</div><!-- /.page-wrapper -->
    <?
    echo $this->Html->script('jquery');
    echo $this->Html->script('bootstrap');
    echo $this->Html->script('bootstrap-select.min');
    echo $this->Html->script('tooltip');
    echo $this->Html->script('collapse');
    echo $this->Html->script('transition');
    echo $this->Html->script('superlist');
    echo $this->fetch('script');
    ?>
</body>
</html>
