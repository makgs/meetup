<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0.8, minimum-scale=0.5, maximum-scale=1">
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php   
        echo $this->Html->css('font-awesome.min');
        echo $this->Html->css('bootstrap-select.min');
        echo $this->Html->css('superlist');
        echo $this->Html->css('bootstrap-datetimepicker.min');
        echo $this->Html->css('bootstrap');
        echo $this->Html->css('my'); 
        echo $this->Html->css('http://fonts.googleapis.com/css?family=Nunito:300,400,700');
		echo $this->fetch('meta');
		echo $this->fetch('css');
	?>
</head>
<body>
    <div class="page-wrapper">
        
        <header class="header header-minimal">
            <div class="header-wrapper">
                <div class="container">
                    <div class="header-inner">
                        <div class="header-logo">
                            <a href="/">
                                <i class="fa fa-users"></i>
                                <span>Yourmeetup</span>
                            </a>
                        </div><!-- /.header-logo -->
                        
                        <div class="header-content">
                            <div class="header-bottom">
                                <ul class="header-nav-primary nav nav-pills collapse navbar-collapse hidden-lg hidden-md">
                                    <li>
                                    <?php echo $this->Html->link(
                                        'Профиль', 
                                        array(
                                            'controller' => 'users',
                                            'action' => 'index',
                                            'admin' => false,
                                            $this->Session->read('Auth.User.vk_key')
                                        )
                                    );?>
                                    </li>
                                    <li>
                                        <?php echo $this->Html->link(
                                            'Создать событие', 
                                            array(
                                                'controller' => 'events',
                                                'action' => 'add',
                                                'admin' => true
                                            )
                                        );?>
                                    </li>
                                    <li>
                                     <?php echo $this->Html->link(
                                            'Управление событиями', 
                                            array(
                                                'controller' => 'events',
                                                'action' => 'active',
                                                'admin' => true
                                            )
                                        );?>  
                                    </li>  
                                    <li>
                                     <?php echo $this->Html->link(
                                            'Избранное', 
                                            array(
                                                'controller' => 'events',
                                                'action' => 'subscriptions',
                                                'admin' => true
                                            )
                                        );?>  
                                    </li>                                       
                                    <li>
                                        <?php echo $this->Html->link(
                                            'Выйти', 
                                            array(
                                                'controller' => 'users',
                                                'action' => 'logout',
                                                'admin' => false
                                            )
                                        );?>  
                                    </li>
                                </ul>

                                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav-primary">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                            </div><!-- /.header-bottom -->
                        </div><!-- /.header-content -->
                    </div><!-- /.header-inner -->
                </div><!-- /.container -->
                
            </div><!-- /.header-wrapper -->

            <div class="header-statusbar">
                <div class="container-fluid">
                    <div class="header-statusbar-inner">
                        <div class="header-statusbar-left">
                            <h1 class="menu_line">Меню пользователя</h1>
                        </div><!-- /.header-statusbar-left -->

                        <div class="header-statusbar-right">
                            <div class="hidden-sm visible-lg visible-md">
                                Навигация: &nbsp;
                                <ul class="breadcrumb">
                                    <li>
                                        <?php echo $this->Html->link(
                                            'Профиль', 
                                            array(
                                                'controller' => 'users',
                                                'action' => 'index',
                                                'admin' => false,
                                                $this->Session->read('Auth.User.vk_key')
                                            )
                                        );?>
                                    </li>
                                    <li>
                                        <?php echo $this->Html->link(
                                            'Создать событие', 
                                            array(
                                                'controller' => 'events',
                                                'action' => 'add',
                                                'admin' => true
                                            )
                                        );?>
                                    </li>
                                    <li>
                                        <?php echo $this->Html->link(
                                            'Управление событиями', 
                                            array(
                                                'controller' => 'events',
                                                'action' => 'active',
                                                'admin' => true
                                            )
                                        );?>                                
                                    </li>
                                    <li>
                                        <?php echo $this->Html->link(
                                            'Избранное', 
                                            array(
                                                'controller' => 'events',
                                                'action' => 'subscriptions',
                                                'admin' => true
                                            )
                                        );?>                                
                                    </li>
                                    <li>
                                        <?php echo $this->Html->link(
                                            'Выйти', 
                                            array(
                                                'controller' => 'users',
                                                'action' => 'logout',
                                                'admin' => false
                                            )
                                        );?>  
                                    </li>
                                </ul>
                            </div>
                        </div><!-- /.header-statusbar-right -->
                    </div><!-- /.header-statusbar-inner -->
                </div><!-- /.container -->
            </div><!-- /.header-statusbar -->
        </header><!-- /.header -->




        <div class="main">
            <div class="outer-admin">
                <div class="wrapper-admin">
                    <div class="content-admin">
                        <div class="content-admin-wrapper">
                            <div class="content-admin-main">
                                <div class="content-admin-main-inner">
                                    <div class="container">
                                        <div class="row">
                                            <?php echo $this->fetch('content'); ?>
                                        </div>
                                    </div><!-- /.container-fluid -->
                                </div><!-- /.content-admin-main-inner -->
                            </div><!-- /.content-admin-main -->

                            <div class="content-admin-footer">
                                <div class="container-fluid">
                                    <div class="content-admin-footer-inner">
                                        &copy; 2015
                                    </div><!-- /.content-admin-footer-inner -->
                                </div><!-- /.container-fluid -->
                            </div><!-- /.content-admin-footer  -->
                        </div><!-- /.content-admin-wrapper -->
                    </div><!-- /.content-admin -->
                </div><!-- /.wrapper-admin -->
            </div><!-- /.outer-admin -->
        </div><!-- /.main -->
    </div><!-- /.page-wrapper -->
            
    <?echo $this->Html->script('jquery');
    echo $this->Html->script('bootstrap');
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('bootstrap-datetimepicker');
    echo $this->Html->script('bootstrap-datetimepicker.ru');
    echo $this->Html->script('datetimepicker_active');
    echo $this->Html->script('bootstrap-select.min');
    echo $this->Html->script('tooltip');
    echo $this->Html->script('collapse');
    echo $this->Html->script('transition');
    echo $this->Html->script('superlist');

    echo $this->fetch('script');?>
</body>
</html>
