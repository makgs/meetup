<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="background-white p20 mb50">
                <h2 class="page-title">Таблица пользователей</h2>
                
                <table class="table table-hover mb0">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Имя</th>
                            <th>Фамилия</th>
                            <th>VK</th>
                            <th>Email</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?foreach($users as $user) {?>
                            <?$user = $user['User'];?>
                            <tr>
                                <th scope="row"><?echo $user['id'];?></th>
                                <td><?echo $user['first_name'];?></td>
                                <td><?echo $user['last_name'];?></td>
                                <td><a href="http://vk.com/id<?echo $user['vk_key'];?>"><?echo $user['vk_key'];?></a></td>
                                <td><?echo $user['email'];?></td>
                                <td class="delete">
                                    <?php echo $this->Form->postLink(
                                        'Обзор', 
                                        array(
                                            'controller' => 'users',
                                            'action' => 'index',
                                            'admin' => false,
                                            $user['vk_key']
                                        )
                                    );?>
                                </td>
                                <td class="delete">
                                    <?php echo $this->Form->postLink(
                                        'Удалить', 
                                        array(
                                            'controller' => 'users',
                                            'action' => 'delete',
                                            'admin' => true,
                                            $user['id']
                                        ),
                                        array('confirm' => 'Вы действительно хотите удалить пользователея?')
                                    );?>
                                </td>
                            </tr>
                        <?}?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><!-- /.container-fluid -->
<? if ($this->Paginator->counter(array('format' => '%pages%')) != 1) { ?>
    <div class="pager">
        <ul>
            <?echo $this->Paginator->prev(
                '<i class="fa fa-arrow-left"></i>', 
                array(
                    'escape'=>false,
                    'tag' => 'li'
                ), 
                null, 
                array(
                    'escape'=>false, 
                    'tag' => 'li',
                    'class' => 'disabled',
                    'disabledTag' => 'a
                ')
            );?>
            <?echo $this->Paginator->numbers(
                array(
                    'separator' => '',
                    'currentTag' => 'a', 
                    'currentClass' => 'active',
                    'tag' => 'li','first' => 1
                )
            );?>
            <?echo $this->Paginator->next(
                '<i class="fa fa-arrow-right"></i>', 
                array(
                    'escape'=>false, 
                    'tag' => 'li',
                    'currentClass' => 'disabled'
                ), 
                null, 
                array(
                    'escape'=>false,
                    'tag' => 'li',
                    'class' => 'disabled',
                    'disabledTag' => 'a'
                )
            );?>
        </ul>
    </div><!-- /.pagination -->
<? } ?>