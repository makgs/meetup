<div class="container">
    <div class="row">
        <div class="col-sm-4 col-lg-3">
            <div class="sidebar">
                <div class="widget">
                    <div class="user-photo">
                        <?foreach ($users as $user) {?>
                        <?if (empty($user['photo'])) {?>
                            <? echo $this->Html->image('default_bg.png'); ?>
                        <?} else {?>
                            <? echo $this->Html->image($user['photo']); ?>
                        <?}?>
                    </div><!-- /.user-photo -->
                </div><!-- /.widget -->

            </div><!-- /.sidebar -->
        </div><!-- /.col-* -->
        
        <div class="col-sm-8 col-lg-9">
            <div class="content">
                <div class="page-title">
                    <h1>Профиль</h1>
                </div><!-- /.page-title -->

                <div class="background-white p20 mb30">
                    <h3 class="page-title">
                        Информация о пользователе
                    </h3>
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label>Имя</label>
                                <p><? echo $user['first_name'];?></p>
                            </div><!-- /.form-group -->

                            <div class="form-group col-sm-6">
                                <label>Фамилия</label>
                                <p><? echo $user['last_name'];?></p>
                            </div><!-- /.form-group -->

                            <div class="form-group col-sm-6">
                                <label>Страница Vk</label>
                                <a href ="http://vk.com/id<? echo $user['vk_key']; ?>" class="my_link"><p>http://vk.com/id<? echo $user['vk_key'];?></p></a>
                            </div><!-- /.form-group -->
                            
                            <div class="form-group col-sm-6">
                                <label>Пол</label>

                                <p><?
                                if(!empty($user['gender'])) {
                                    if ($user['gender'] == 'm') {
                                        echo 'Мужской';
                                    } else {
                                        echo 'Женский';
                                    }
                                } else {
                                    echo '<p>Пол неизвестен</p>';
                                }
                                    ?></p>

                            </div><!-- /.form-group -->
                        </div><!-- /.row -->
                    <?}?>
                </div>
            </div><!-- /.content -->
        </div><!-- /.col-* -->
    </div><!-- /.row -->

    <div class="background-white p20 mb30">
        <div class="row">
            <div class="col-md-12">
                <div class="wrapper">
                    <div class="row">
                        <div class="tabs">
                            <div class="col-md-6">
                                <h2 class="tab">Опубликованные события</h2>
                            </div>
                            <div class="col-md-6">
                                <h2 class="tab">Избранное</h2>
                            </div>   
                        </div>
                    </div><!-- /.row -->

                    <div class="row">
                        <div class="tab_content">
                            <div class="tab_item">
                                <div class="widget">
                                    <h2 class="widgettitle widget_profile">
                                        <?if (empty($yourEvents))
                                            echo "Нет опубликованных мероприятий";
                                        ?>
                                    </h2>

                                    <?foreach ($yourEvents as $event) {?>
                                        <?$event = $event['Event']?>
                                        <div class="col-md-3">
                                            <div class="cards-small">
                                                <div class="card-small">
                                                    <div class="card-small-image">
                                                        <a href="/events/view/<?echo $event['id']; ?>">
                                                            <?if (isset($event['photo'])) {?>
                                                                <? echo $this->Html->image('/files/event/photo/' . $event['photo_dir'] . '/small_' . $event['photo']); ?>
                                                            <?} else {?>
                                                                <? echo $this->Html->image('/img/default_bg.png'); ?>
                                                            <?}?>
                                                        </a>
                                                    </div><!-- /.card-small-image -->

                                                    <div class="card-small-content profile">
                                                        <h3><a href="/events/view/<?echo $event['id']; ?>"><?echo $event['name']?></a></h3>
                                                        <h4><?echo $event['city']?></h4>

                                                        <?if ($event['date_start'] != null) {?>
                                                            <? echo $this->Date->makeDateAndTime(
                                                            $event['date_start']
                                                        );?>
                                                        <?}?>
                                                    </div><!-- /.card-small-content -->
                                                </div><!-- /.card-small -->
                                            </div><!-- /.cards-small -->
                                        </div>
                                    <?}?>
                                </div><!-- /.widget -->                            
                            </div><!-- /.tab_item -->

                            <div class="tab_item">
                                <div class="widget">
                                    <h2 class="widgettitle widget_profile">
                                        <?if (empty($pubEvents))
                                            echo "Нет избранных событий";
                                        ?>
                                    </h2>
            
                                    
                                    <?foreach ($pubEvents as $event) {?>
                                    <?$event = $event['Event']?>
                                        <div class="col-md-3">
                                            <div class="cards-small">
                                                <div class="card-small">
                                                    <div class="card-small-image">
                                                        <a href="/events/view/<?echo $event['id']; ?>">
                                                            <?if (isset($event['photo'])) {?>
                                                                <? echo $this->Html->image('/files/event/photo/' . $event['photo_dir'] . '/small_' . $event['photo']); ?>
                                                            <?} else {?>
                                                                <? echo $this->Html->image('/img/default_bg.png'); ?>
                                                            <?}?>
                                                        </a>
                                                    </div><!-- /.card-small-image -->
                                                    
                                                    <div class="card-small-content profile">
                                                        <h3><a href="/events/view/<?echo $event['id']; ?>"><?echo $event['name']?></a></h3>
                                                        <h4><?echo $event['city']?></h4>
                                                        <?if ($event['date_start'] != null) {?>
                                                            <? echo $this->Date->makeDateAndTime(
                                                            $event['date_start'] 
                                                        );?>
                                                        <?}?>
                                                    </div><!-- /.card-small-content -->
                                                </div><!-- /.card-small -->
                                            </div><!-- /.cards-small -->
                                        </div>
                                    <?}?>
                                    
                                </div><!-- /.widget -->                            
                            </div><!-- /.tab_item -->  
                        </div><!-- /.tab_content -->
                    </div><!-- /.row -->
                </div><!-- /.wrapper -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.background-white -->
</div><!-- /.container -->