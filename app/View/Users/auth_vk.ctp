
<div class="container">
    <h2>Что-то пошло не так!</h2>
    <?php if (isset($errorMessage)) { ?>
        <p><?php echo $errorMessage ?></p>
    <?php } ?>

    <?php echo $this->Html->link(
        'Попробуйте еще раз', 
        array(
            'controller' => 'users',
            'action' => 'auth_vk',
            'admin' => false
        )
    );?>
</div>