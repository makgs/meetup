<?php
App::uses('AppController', 'Controller');
/**
 * Actions Controller
 *
 * @property Action $Action
 */
class ActionsController extends AppController {
    
    var $uses = array('Event', 'Action');
    
    public function isAuthorized($user) {
        
		if ($this->action === 'add') {
			return true;
		}
        
        if (in_array($this->action, array('admin_approve', 'admin_delete', 'admin_unapprove'))) {
            
            $actionId = (int) $this->request->params['pass'][0];
            $options = array(
                'conditions' => array(
                    'Action.' . $this->Action->primaryKey => $actionId
                ),
                'recursive' => -1,
                'fields' => array('Action.event_id'),
            );
            $eventId = $this->Action->find('first', $options);
            $eventId = $eventId['Action']['event_id'];
            
            /**
             * uses from redirect in $this->admin_approve() and $this->admin_delete()
             */
            $this->eventId = $eventId;
            
            if ($this->Event->isOwnedBy($eventId, $user['id'])) {
                return true;
            } else {
                throw new ForbiddenException();
            }
        }

        return parent::isAuthorized($user);
    }
    /**
     * add method
     *
     * @return void
     */
	public function add() {
        
		$this->request->allowMethod('post');
            
        $this->Action->create();
        $this->request->data['Action']['is_approved']   = 0;
        $this->request->data['Action']['user_id']       = $this->Auth->user('id');
        $this->request->data['Action']['voting_count']  = 0;
        
        if ($this->Action->save($this->request->data) === false) {
            
            $this->Session->write(
                'Action.validationErrors',
                $this->Action->validationErrors
            );
            
        }
        
        return $this->redirect(
            array(
                'controller' => 'events',
                'action' => 'view', 
                $this->request->data['Action']['event_id']
            )
        );

	}
    /**
     * delete method
     */
    public function admin_delete($id = null) {
        
        $this->request->allowMethod('post', 'delete');
        $this->Action->id = $id;
        $this->Action->delete();
        
        /**
         * set in $this->isAuthorized()
         */
        $eventId = $this->eventId;
        
        return $this->redirect(array('controller' => 'events', 'action' => 'view', 'admin' => false, $eventId));
    }
    /**
     * method approve action
     */
    public function admin_approve($id = null) {
        
        $this->request->allowMethod('post');
        
        $data = array('id' => $id, 'is_approved' => 1);
        $this->Action->save($data);
        
        /**
         * set in $this->isAuthorized()
         */
        $eventId = $this->eventId;
        
        return $this->redirect(array('controller' => 'events', 'action' => 'view', 'admin' => false, $eventId));
    }
    
    public function admin_unapprove($id = null) {
        
        $this->request->allowMethod('post');
        
        $data = array('id' => $id, 'is_approved' => 0);
        $this->Action->save($data);
        
        /**
         * set in $this->isAuthorized()
         */
        $eventId = $this->eventId;
        
        return $this->redirect(array('controller' => 'events', 'action' => 'view', 'admin' => false, $eventId));
    }     
}
