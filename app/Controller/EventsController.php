<?php
App::uses('AppController', 'Controller');
/**
 * Events Controller
 *
 * @property Event $Event
 * @property PaginatorComponent $Paginator
 */
class EventsController extends AppController {

    var $uses = array('Event', 'Action', 'Voting', 'Subscription', 'User');
    
/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator');

    public function isAuthorized($user) {
        
        if ($this->action === 'admin_add' 
        || $this->action === 'admin_active' 
        || $this->action === 'admin_subscriptions') {
            
            return true;
        }
        
        if (in_array($this->action, array('admin_edit', 'admin_delete', 'admin_publish'))) {
            
            $eventId = (int) $this->request->params['pass'][0];
            
            if ($this->Event->isOwnedBy($eventId, $user['id'])) {
                return true;
            } 
        }

        return parent::isAuthorized($user);
    }
    
/**
 * View method of one method
 *
 * @throws NotFoundException
 * @param string $id If of event
 * @return void
 */
    public function view($id = null) {
        
        $this->helpers[] = 'Date';        
        $user_id = $this->Auth->user('id');
        $this->set('user_id', $user_id);

        if (!$this->Event->exists($id)) {
            
            throw new NotFoundException();
        }
        
        $optionsEvent = array(
            'conditions' => array(
                'Event.' . $this->Event->primaryKey => $id
            ),
            'recursive' => -1
        );
        $event = $this->Event->find('first', $optionsEvent);
        

        if (($event['Event']['is_public'] != 1)
        && !$this->Event->isOwnedBy($id, $this->Auth->user('id')) 
        && $this->Auth->user('role') != 'admin') {
            
            throw new NotFoundException();
            
        }
        $this->set('event', $event);
        $this->set('title_for_layout', $event['Event']['name']);

        $author_id = $event['Event']['user_id'];

        $optionUserInfo = array(
            'conditions' => array(
                'User.id' => $author_id),
            'recursive' => -1
        );
        
        $author = $this->User->find('first', $optionUserInfo);
        $user_info = ($author['User']['first_name'] . ' ' . $author['User']['last_name']);
        $this->set('user_info', $user_info);

        $user_key = $author['User']['vk_key'];
        $this->set('user_key', $user_key);

        $optionsApprovedActions = array(
            'conditions' => array(
                'Action.event_id' => $id,
                'Action.is_approved' => 1
            ),
            'recursive' => -1
        );        
        $approvedActions = $this->Action->find('all', $optionsApprovedActions);
        if(isset($user_id)) {

            foreach($approvedActions as $key => $value){
                
                $optionsIsLiked = array(
                    'conditions' => array(
                        'Voting.action_id' => $value['Action']['id'],
                        'Voting.user_id' => $user_id
                    ),
                    'recursive' => -1
                );
                $isLiked = $this->Voting->find('first', $optionsIsLiked);
                
                if (!empty($isLiked)){
                    $approvedActions[$key]['Action']['is_liked'] = true;
                } else {
                    $approvedActions[$key]['Action']['is_liked'] = false;
                }
            }
        }
        $this->set('approvedActions', $approvedActions);
        
        $optionsUnApprovedActions = array(
            'conditions' => array(
                'Action.event_id' => $id,
                'Action.is_approved' => 0
            ),
            'recursive' => -1
        );
        $unApprovedActions = $this->Action->find('all', $optionsUnApprovedActions);
        if(isset($user_id)) {
            
            foreach($unApprovedActions as $key => $value){
                
                $optionsIsLiked = array(
                    'conditions' => array(
                        'Voting.action_id' => $value['Action']['id'],
                        'Voting.user_id' => $user_id
                    ),
                    'recursive' => -1
                );
                $isLiked = $this->Voting->find('first', $optionsIsLiked);
                
                if (!empty($isLiked)){
                    $unApprovedActions[$key]['Action']['is_liked'] = true;
                } else {
                    $unApprovedActions[$key]['Action']['is_liked'] = false;
                }
            }
        }
        $this->set('unApprovedActions', $unApprovedActions);        
        
        if ($this->Event->isOwnedBy($id, $this->Auth->user('id'))){            
            $this->set('eventOwnedByUser', true);
        } else {
            $this->set('eventOwnedByUser', false);
        }

        if ($this->Session->check('Action.validationErrors')) {
            
            $this->set(
                'validationErrors',
                $this->Session->read('Action.validationErrors')
            );
            $this->Session->delete('Action.validationErrors');            
        }
        
        $sideBarEvents = $this->Event->getSideBarEvents($id);
        $this->set('sideBarEvents', $sideBarEvents);
        
        $isSubscription = $this->Subscription->isSubscription($id, $user_id);
        
        $this->set('isSubscription', $isSubscription);
    }
/**
 * add method
 *
 * @return void
 */
    public function admin_add() {
        
        $this->set('title_for_layout', 'Создание события');
        
        if ($this->request->is('post')) {
            $this->Event->create();
            $this->request->data['Event']['user_id'] = $this->Auth->user('id');
            $this->request->data['Event']['is_finished'] = 0;
            if (isset($this->request->data['Event']['is_banned'])) {
                
                throw new ForbiddenException();
                
            }
            if (!empty($this->request->data['Event']['date_start'])) {
                $this->request->data['Event']['date_start'] =
                    date('Y-m-d H:i', strtotime($this->request->data['Event']['date_start']));
            }
            if (!empty($this->request->data['Event']['date_end'])) {
                $this->request->data['Event']['date_end'] =
                    date('Y-m-d H:i', strtotime($this->request->data['Event']['date_end']));
            }
            $this->Event->save($this->request->data);
            
            $i = 0;
            $data = null;
            if (isset($this->request->data['dt'])) { 
            
                $numDiscussActions = count($this->request->data['dt']['name']); 
                
                for ($i = 1; $i <= $numDiscussActions; ++$i) {
                    $data[$i] = array(
                        'name' => $this->request->data['dt']['name'][$i],
                        'description' => $this->request->data['dt']['text'][$i],
                        'user_id' => $this->Auth->user('id'),
                        'event_id' => $this->Event->id,
                        'voting_count' => '0'
                    );
                }
            }
                
            if (isset($this->request->data['ft'])) {
                    
                $numFixActions = count($this->request->data['ft']['name']);
                   
                for ($j = 1; $j <= $numFixActions; ++$j, ++$i) {
                    $data[$i] = array(
                        'name' => $this->request->data['ft']['name'][$j],
                        'description' => $this->request->data['ft']['text'][$j],
                        'user_id' => $this->Auth->user('id'),
                        'event_id' => $this->Event->id,
                        'is_approved' => true,
                        'voting_count' => '0'
                    );
                }
            }
            
            $this->Action->saveMany($data, array('deep' => true));
            
            if (empty($this->Event->validationErrors)) {
                
                return $this->redirect(
                    array(
                        'action' => 'view/' . $this->Event->id . '', 
                        'admin' => false
                    )
                );
            }
        }
        $users = $this->Event->User->find('list');
        $this->set(compact('users'));
        
        $options = array(
            'recursive' => 1
        );
        $event = $this->Event->find('first', $options);
        $this->set('event', $event);
    }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_edit($id = null) {
        
        $this->set('title_for_layout', 'Редактирование события');
        
        if ($this->request->is(array('post', 'put'))) {
            $this->Event->id = $id;
            if (isset($this->request->data['Event']['is_banned'])) {
                
                throw new ForbiddenException();
                
            }
            if (!empty($this->request->data['Event']['date_start'])) {
                $this->request->data['Event']['date_start'] =
                    date('Y-m-d H:i', strtotime($this->request->data['Event']['date_start']));
            }
            if (!empty($this->request->data['Event']['date_end'])) {
                $this->request->data['Event']['date_end'] =
                    date('Y-m-d H:i', strtotime($this->request->data['Event']['date_end']));
            }
            $this->Event->save($this->request->data);
            
            $arr = $this->Action->findAllByEvent_id($id);
            
            for($i = 0; $i < count($arr); ++$i) {
                if ($arr[$i]['Action']['is_approved'] == '0') {
                    $index = $arr[$i]['Action']['id'];
                    if(isset($this->request->data['change_dt']['name'][$index])) {
                        
                        $data[$index] = array(
                            'name' => $this->request->data['change_dt']['name'][$index],
                            'description' => $this->request->data['change_dt']['text'][$index],
                            'user_id' => $this->Auth->user('id'),
                            'event_id' => $this->Event->id
                        );
                    } else {
                        $this->Action->id = $index;
                        $this->Action->delete();
                        continue;
                    }
                }
                if ($arr[$i]['Action']['is_approved'] == '1') {
                    $index = $arr[$i]['Action']['id'];
                    if( isset($this->request->data['change_ft']['name'][$index])) {
                        $data[$index] = array(
                            'name' => $this->request->data['change_ft']['name'][$index],
                            'description' => $this->request->data['change_ft']['text'][$index],
                            'user_id' => $this->Auth->user('id'),
                            'event_id' => $this->Event->id,
                            'is_approved' => true
                        );
                    } else {
                        $this->Action->id = $index;
                        $this->Action->delete();
                        continue;
                    }
                }
                $this->Action->id = $index;
                $this->Action->save($data[$index]);
            }
            
            unset($data);
            
            $i = 0;
            if (isset($this->request->data['dt'])) { 
            
                $numDiscussActions = count($this->request->data['dt']['name']); 
                
                for ($i = 1; $i <= $numDiscussActions; ++$i) {
                    $data[$i] = array(
                        'name' => $this->request->data['dt']['name'][$i],
                        'description' => $this->request->data['dt']['text'][$i],
                        'user_id' => $this->Auth->user('id'),
                        'event_id' => $this->Event->id,
                        'voting_count' => '0'
                    );
                }
            }
                
            if (isset($this->request->data['ft'])) {
                    
                $numFixActions = count($this->request->data['ft']['name']);
                    
                for ($j = 1; $j <= $numFixActions; ++$j, ++$i) {
                    $data[$i] = array(
                        'name' => $this->request->data['ft']['name'][$j],
                        'description' => $this->request->data['ft']['text'][$j],
                        'user_id' => $this->Auth->user('id'),
                        'event_id' => $this->Event->id,
                        'is_approved' => true,
                        'voting_count' => '0'
                    );
                }
            }
            if (isset($data))
                $this->Action->saveMany($data, array('deep' => true));
            
            if (empty($this->Event->validationErrors)) {
                
                return $this->redirect(
                    array(
                        'action' => 'view/' . $this->Event->id . '', 
                        'admin' => false
                    )
                );
            } 
        } else {
            $options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
            $this->request->data = $this->Event->find('first', $options);
        }
        
        $options = array(
            'conditions' => array(
                'Event.' . $this->Event->primaryKey => $id
            ),
            'recursive' => 1
        );
        $event = $this->Event->find('first', $options);
        $this->set('event', $event);
    }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_delete($id = null) {

        $this->Event->id = $id;
        
        if (!$this->Event->exists()) {
            
            throw new NotFoundException();
        
        }
        
        $this->request->allowMethod('post', 'delete');
        $this->Event->delete();
        $this->Action->deleteAll(array('Action.event_id' => $id), false);
        
        $refer = $this->request->referer('/', true);
        if (strripos($refer, '/events/view/') !== false) {
            $refer = '/';
        }

        return $this->redirect($refer);
    }
/**
 * view all user events
 *
 * @return void
 */
    public function admin_active() {
        
        $this->paginate = array(
            'limit' => 10
        );
        
        $this->set('title_for_layout', 'Мои события');
        $this->helpers[] = 'Date';       
        $options = array(
            'conditions' => array(
                'Event.is_finished' => 0,
                'Event.user_id' => $this->Auth->user('id')
            ),
            'recursive' => -1,
            'order' => array('Event.date_start' => 'desc'),
            'limit' => 10
        );
        $this->Paginator->settings = $options;
        $events = $this->paginate();
        
        $this->set('events', $events);   
    }

    public function view_all($id = null) {
        
        $this->helpers[] = 'Date';
        
        $this->paginate = array(
            'limit' => 10
        );
        
        $today = date("Y-m-d H:i:s");
        if ($id == 'old') {
            
            $this->set('title_for_layout', 'Прошедшие события');
            
            $oldOptions = array(
                'recursive' => -1,
                'conditions' => array(
                    'Event.is_public' => 1,
                    'Event.date_start <' => $today
                ),
                'order' => array('Event.date_start' => 'desc'),
                'limit' => 10
            );
            
            $this->Paginator->settings = $oldOptions;
            $oldEvents = $this->paginate();
            $this->set('Events', $oldEvents);
            
        } elseif ($id == 'fresh') {
            
            $this->set('title_for_layout', 'Ближайшие события');
            
            $freshOptions = array(
                'recursive' => -1,
                'conditions' => array(
                    'Event.is_public' => 1,
                    'Event.date_start >' => $today
                ),
                'order' => array('Event.date_start' => 'asc'),
                'limit' => 10
            );
            
            $this->Paginator->settings = $freshOptions;
            $freshEvents = $this->paginate();
            $this->set('Events', $freshEvents);
            
        } else {
            $this->set('Events', NULL);
        }
    }
    
    function admin_publish($id = null, $ban = null){
        
        $this->Event->id = $id;

        $options = array(
            'conditions' => array(
                'Event.id' => $id
            ),
            'recursive' => -1
        );
        $event = $this->Event->find('first', $options);
        
        $event = $event['Event'];
        if ($event['is_banned'] == 0) {
            if ($event['is_public'] == 1)
                $event['is_public'] = 0;
            elseif ($event['is_public'] == 0)
                $event['is_public'] = 1;
        } else 
             $event['is_public'] = 0;
        
        $this->Event->save($event);
        
        $this->redirect($this->request->referer('/', true));
    }
    
    function admin_ban($id = null) {
        
        $this->Event->id = $id;
        
        $options = array(
            'conditions' => array(
                'Event.id' => $id
            ),
            'recursive' => -1
        );
        
        $event = $this->Event->find('first', $options);
        $event = $event['Event'];
        
        if ($event['is_banned'] == 0) {
            $event['is_banned'] = 1;  
            $event['is_public'] = 0;            
        } elseif($event['is_banned'] == 1) {
            $event['is_banned'] = 0; 
        }
        
        $this->Event->save($event);
        $this->redirect($this->request->referer('/', true));
    }
    
/**
 * view all user subscriptions
 *
 * @return void
 */
    public function admin_subscriptions() {
        
        $this->paginate = array(
            'limit' => 10
        );
        
        $this->set('title_for_layout', 'Подписки');
        $this->helpers[] = 'Date';       
        $options = array(
            'conditions' => array(
                'Event.is_public' => 1,
                'Event.id' => 
                    $this->Subscription->getUserSubscription(
                        $this->Auth->user('id')
                    ),
            ),
            'recursive' => 1,
            'order' => array('Event.date_start' => 'desc'),
            'limit' => 10
        );
        $this->Paginator->settings = $options;
        $events = $this->paginate();
        $this->set('events', $events);
    }
    
    public function admin_index() {
        
        $this->set('title_for_layout', 'Панель администратора');
        
        $options = array(
            'recursive' => 0,
            'limit' => 10,
            'order' => array(
                'Event.' . $this->Event->primaryKey => 'desc'
            )
        );
        
        $this->Paginator->settings = $options;
        $events = $this->paginate();
        $this->set('events', $events);
    }
}