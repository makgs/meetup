<?php
App::uses('AppController', 'Controller');
/**
 * Subscriptions Controller
 *
 */
class SubscriptionsController extends AppController {

	var $uses = array('Subscription', 'Event');
    
    public function isAuthorized($user) {
        
        if ($this->action === 'subscribe') {
            return true;
        }
        
        return parent::isAuthorized($user);
    }
    
/**
 * subscribe or unsubscribe auth user 
 *
 * @return void
 */
	public function subscribe() {
        
        $this->layout = null ;
		$user_id = $this->Auth->user('id');
        $this->request->allowMethod('post', 'delete');

        if ($user_id > 0) {
            
            $event_id = $_POST['event'];
            $subscriptionOptions = array(
                'conditions' => array(
                    'Subscription.user_id' => $user_id,
                    'Subscription.event_id' => $event_id
                ),
                'recursive' => -1
            );
            $array_event = $this->Subscription->find('first', $subscriptionOptions);

            if (empty($array_event)) {
                $this->Subscription->create();
                $this->request->data['Subscription']['user_id'] = $user_id;
                $this->request->data['Subscription']['event_id'] = $event_id;
                $this->Subscription->save($this->request->data);
            } else {
                $id_subscription = $array_event['Subscription']['id'];
                $this->Subscription->delete($id_subscription);
            }

            $eventOption = array(
                'conditions' => array(
                    'Event.id' => $event_id
                ),
                'recursive' => -1
            );
            $all_event = $this->Event->find('first', $eventOption);
            
            $this->set(compact('all_event'));
            
        }

	}

}
