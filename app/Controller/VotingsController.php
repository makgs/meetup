<?php
App::uses('AppController', 'Controller');
/**
 * Votings Controller
 *
 * @property Voting $Voting
 * @property PaginatorComponent $Paginator
 */
class VotingsController extends AppController {

	var $uses = array('Voting', 'Action');

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index($id = null) {
		$user_id = $this->Auth->user('id');
		$this->Voting->recursive = 0;
		$this->set('votings', $this->Paginator->paginate());
		$this->Voting->find('list');
		$this->layout = 'ajax';
		$action_id = 16;
		$actionOption = array(
			'conditions' => array(
			'Action.id' => $action_id
			),
			'recursive' => -1
		);
		$all_action = $this->Action->find('first', $actionOption);

        if ($this->request->is('post')) {

        	if ($user_id > 0) {
        		$action_id = $_POST['param'];
				$votingOptions = array(
            		'conditions' => array(
                		'Voting.user_id' => $user_id,
                		'Voting.action_id' => $action_id
            		),
            		'recursive' => -1
            	);
        		$array_action = $this->Voting->find('first', $votingOptions);

        		if (empty($array_action)) {
        			$this->Voting->create();
        			$this->request->data['Voting']['user_id'] = $user_id;
        			$this->request->data['Voting']['action_id'] = $action_id;
        			$this->Voting->save($this->request->data);
        		} else {
        			$this->request->allowMethod('post', 'delete');
        			$id_voting = $array_action['Voting']['id'];
					$this->Voting->delete($id_voting);
				}

				$actionOption = array(
					'conditions' => array(
						'Action.id' => $action_id
					),
					'recursive' => -1
				);
				$all_action = $this->Action->find('first', $actionOption);
				echo json_encode(array('action_id'=> $action_id, 'voting_count'=> $all_action['Action']['voting_count']));
			}
		}
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Voting->exists($id)) {
			throw new NotFoundException(__('Invalid voting'));
		}
		$options = array('conditions' => array('Voting.' . $this->Voting->primaryKey => $id));
		$this->set('voting', $this->Voting->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Voting->create();
			if ($this->Voting->save($this->request->data)) {
				//$this->Flash->success(__('The voting has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				//$this->Flash->error(__('The voting could not be saved. Please, try again.'));
			}
		}
		$users = $this->Voting->User->find('list');
		$actions = $this->Voting->Action->find('list');
		$this->set(compact('users', 'actions'));
	}


/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Voting->id = $id;
		if (!$this->Voting->exists()) {
			throw new NotFoundException(__('Invalid voting'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Voting->delete()) {
			//$this->Flash->success(__('The voting has been deleted.'));
		} else {
			//$this->Flash->error(__('The voting could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
