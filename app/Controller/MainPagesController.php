<?php
/**
 * Main Pages Controller
 */
App::uses('AppController', 'Controller');

class MainPagesController extends AppController {

    var $uses = array('Event');
    
/**
 * Home page
 */    
    public function index() {
        
        $today = date("Y-m-d H:i:s");
        
        $oldOptions = array(
            'limit' => 3,
            'recursive' => -1,
            'conditions' => array(
                'Event.is_public' => 1,
                'Event.date_start <' => $today
            ),
            'order' => array('Event.date_start DESC')
        );
        
        $oldEvents = $this->Event->find('all', $oldOptions);
        $this->set('oldEvents', $oldEvents);
        
        $freshOptions = array(
            'limit' => 3,
            'recursive' => -1,
            'conditions' => array(
                'Event.is_public' => 1,
                'Event.date_start >' => $today
            ),
            'order' => array('Event.date_start')
        );
        
        $freshEvents = $this->Event->find('all', $freshOptions);
        $this->set('freshEvents', $freshEvents);
        
        $this->set('title_for_layout', 'Yourmeetup');
        $this->set('loggedIn', $this->Auth->loggedIn());
    }
}
